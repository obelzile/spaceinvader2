#include "GameEngine.h"

/*
* Entry point
*/
int main()
{
	GameEngine game;
	game.initialise();
    game.run();

    return 0;
}