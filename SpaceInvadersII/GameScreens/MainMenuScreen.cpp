#include "MainMenuScreen.h"
#pragma warning (disable : 4482)

MainMenuScreen::MainMenuScreen(GameEngine *game)
    : GameScreen(game)
{
    mainMenu = NULL;
}


MainMenuScreen::~MainMenuScreen(void)
{
}

bool MainMenuScreen::initialise()
{
    wstring space[] = { L" #####                             ",
                        L"#     # #####    ##    ####  ######",
                        L"#       #    #  #  #  #    # #     ",
                        L" #####  #    # #    # #      ##### ",
                        L"      # #####  ###### #      #     ",
                        L"#     # #      #    # #    # #     ",
                        L" #####  #      #    #  ####  ######"};
    
    wstring invaders[] = {  L"###                                                   #########",
                            L" #  #    # #    #   ##   #####  ###### #####   ####     ## ##  ",
                            L" #  ##   # #    #  #  #  #    # #      #    # #         ## ##  ",
                            L" #  # #  # #    # #    # #    # #####  #    #  ####     ## ##  ",
                            L" #  #  # # #    # ###### #    # #      #####       #    ## ##  ",
                            L" #  #   ##  #  #  #    # #    # #      #   #  #    #    ## ##  ",
                            L"### #    #   ##   #    # #####  ###### #    #  ####   #########"};

    spaceText = new TextBlock(space,7,7);
    invadersText = new TextBlock(invaders,7,7);

    game->engine->Audio->arreterMP3Channel2();

    game->engine->Audio->jouerMP3Channel2(L".\\Sounds\\Opening.mp3",24700);
    
	mainMenu = new MenuGen(game->engine->Display->getCenter().getPositionX(),30);
	mainMenu->addOption(L"NEW GAME");
	mainMenu->addOption(L"HIGH SCORE");
	mainMenu->addOption(L"QUIT");

    mainMenu->setPositionX(game->engine->Display->getCenter().getPositionX()-mainMenu->getWidth()/2);

    return true;
}

bool MainMenuScreen::dispose()
{
    if(mainMenu != NULL)
        delete mainMenu;

    game->engine->Audio->arreterMP3Channel2();
    return true;
}


bool MainMenuScreen::update(int elapsedTick)
{
   mainMenu->update(elapsedTick);

    return true;
}

bool MainMenuScreen::render()
{

    game->engine->Display->displayTextBlock(5,5,spaceText);
    game->engine->Display->displayTextBlock(30,13,invadersText);    
	mainMenu->render(game->engine);

    return true;
}


bool MainMenuScreen::key_pressed(int elapsedTick,int virtualKey)
{

	return true;
}

bool MainMenuScreen::key_released(int elapsedTick,int virtualKey)
{
	mainMenu->key_released(elapsedTick,virtualKey);

    switch(virtualKey)
    {
    case KEY_RETURN_KEY:
        {
            int selection = mainMenu->GetCurrentSelection();
            switch(selection)
            {
            case 0:
                {
					game->setCurrentGameScreen(GameScreenEnum::Intro);
                }
                break;
            case 1:
                {
					game->setCurrentGameScreen(GameScreenEnum::HighScore);
                }
                break;
            case 2:
                {
                    game->setCurrentGameScreen(GameScreenEnum::Quit);
                    break;
                }
            }
        }
        break;
    }
	
	return true;
}