#pragma once
#include <vector>
using namespace std;

#include "../GameEngine.h"
#include "GameScreen.h"

class GamePlayScreen :
    public GameScreen
{
private:
	int pauseTicksCount;
    bool pause;
    int shipDiedWaitTicks;
    int currentshipDiedWaitTicks;
public:
    GamePlayScreen(GameEngine *game);
    ~GamePlayScreen(void);

	bool prochainNiveau();
	bool gameover();
    bool initialise();
    bool dispose();
    bool update(int elapsedTick);
    bool render();
	bool key_pressed(int elapsedTick,int virtualKey);
	bool key_released(int elapsedTick,int virtualKey);

    void afficherInfo();
};

