#pragma once
#include "../ConsoleEngine/ICallbackInput.h"

class GameEngine;

class GameScreen : public ICallbackInput
{
protected:
    GameEngine *game;
    bool screenDone;
	bool initialised;

public:
    GameScreen(GameEngine *game);
    ~GameScreen(void);

    bool isScreenDone() { return screenDone; };
	bool isInitialised() { return initialised; };
    virtual bool initialise() = 0;
    virtual bool dispose() = 0;
    virtual bool update(int elapsedTick) = 0;
    virtual bool render() = 0;
};

