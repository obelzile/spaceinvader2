#pragma once

#include "../GameEngine.h"
#include "GameScreen.h"
#include "../MenuGen.h"

class MainMenuScreen :
    public GameScreen
{
private:
    TextBlock *spaceText;
    TextBlock *invadersText;

	MenuGen *mainMenu;

public:
    MainMenuScreen(GameEngine *game);
    ~MainMenuScreen(void);

    bool initialise();
    bool dispose();
    bool update(int elapsedTick);
    bool render();
	bool key_pressed(int elapsedTick,int virtualKey);
	bool key_released(int elapsedTick,int virtualKey);
};

