#pragma once

#include "GameScreen.h"
#include "../GameEngine.h"

#include "../ConsoleEngine\TextBlock.h"
#include "../BlinkingText.h"

class StartScreen :
    public GameScreen
{
private:
    //35x7
    //52x7
    TextBlock *spaceText;
    TextBlock *invadersText;
    BlinkingText *spaceTextBlink;
    BlinkingText *invadersTextBlink;
    BlinkingText *startTextBlink;

public:
    StartScreen(GameEngine *game);
    ~StartScreen(void);

    bool initialise();
    bool dispose();
    bool update(int elapsedTick);
    bool render();
	bool key_pressed(int elapsedTick,int virtualKey);
	bool key_released(int elapsedTick,int virtualKey);
};

