#pragma once
#include "../GameEngine.h"
#include "GameScreen.h"

class IntroScreen :
	public GameScreen
{
private:
	TextBlock *spaceText;
	TextBlock *invadersText;

	TextBlock *bombariderText;
	TextBlock *mitrailleurText;
	TextBlock *officierText;
	TextBlock *shieldText;
	TextBlock *laserText;

public:
	IntroScreen(GameEngine *game);
	~IntroScreen(void);
	bool initialise();
	bool dispose();
	bool update(int elapsedTick);
	bool render();
	bool key_pressed(int elapsedTick,int virtualKey);
	bool key_released(int elapsedTick,int virtualKey);
};

