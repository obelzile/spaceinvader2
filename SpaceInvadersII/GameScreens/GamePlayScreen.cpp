﻿#include "GamePlayScreen.h"
#include "../ExplosionMartien.h"
#include <vector>
#include <iostream>
#include <sstream>
#include <iomanip>
using namespace std;

GamePlayScreen::GamePlayScreen(GameEngine *game)
    : GameScreen(game)
{
    pause = false;
    shipDiedWaitTicks = 2500;
    currentshipDiedWaitTicks = 0;	
}


GamePlayScreen::~GamePlayScreen(void)
{
}

bool GamePlayScreen::initialise()
{
    game->engine->Audio->arreterMP3Channel2();
    game->score = 0;

    game->nbLives = 3;
	game->setNiveau(1);

    if(game->ship != NULL)
        delete game->ship;
	game->ship = new SpaceShip(game);

    if(game->martienArmy != NULL)
        delete game->martienArmy;
    
    game->martienArmy = new MartienArmy(game);
    game->martienArmy->initialise();

	pauseTicksCount = 1000;

	while(game->explosions.size() !=0)
	{
		delete game->explosions[game->explosions.size()-1];
		game->explosions.pop_back();
	}

	while(game->laserMartiens.size() !=0)
	{
		delete game->laserMartiens[game->laserMartiens.size()-1];
		game->laserMartiens.pop_back();
	}

    return true;
}

bool GamePlayScreen::prochainNiveau()
{
	game->engine->Audio->arreterMP3Channel2();

	game->setNiveau(game->getNiveau()+1);

	if(game->martienArmy != NULL)
		delete game->martienArmy;

	game->martienArmy = new MartienArmy(game);
	game->martienArmy->initialise();

	pauseTicksCount = 1000;

	return true;
}

bool GamePlayScreen::gameover()
{
	game->setCurrentGameScreen(HighScore);
	return true;
}

bool GamePlayScreen::dispose()
{

    return true;
}

bool GamePlayScreen::update(int elapsedTicks)
{
	//update les laser tirés par le vaisseau
	vector<Explosion*>::iterator it = game->explosions.begin();
	while (it != game->explosions.end())
	{
		if(!(*it)->update(elapsedTicks,game->engine))
		{            
			delete (*it);
			it = game->explosions.erase(it);
		}
		else
		{
			it++;
		}
	}

	pauseTicksCount -= elapsedTicks;
	if(pauseTicksCount > 0)
	{
		return true;
	}

	//Délai après être mort
    if(game->ship->getShipDead())
    {
        currentshipDiedWaitTicks += elapsedTicks;
        if(currentshipDiedWaitTicks >= shipDiedWaitTicks)
        {
            currentshipDiedWaitTicks = 0;
            game->ship->setShields(1);
            game->ship->setMaxLaser(1);
            game->ship->setShipDead(false);
        }
        else
        {
            return false;
        }
    }

	//test les collisions du vaisseau 
	game->ship->testCollision();

	//deplacer les lasers martiens
	vector<LaserMartien*>::iterator iterAlienLaser = game->laserMartiens.begin();
	while (iterAlienLaser != game->laserMartiens.end())
	{
        if(!(*iterAlienLaser)->update(elapsedTicks,game->engine))
		{            
			delete (*iterAlienLaser);
			iterAlienLaser = game->laserMartiens.erase(iterAlienLaser);
		}
		else
		{
			iterAlienLaser++;
		}
	}

	if(game->martienArmy->martiens.size() == 0)
	{
		//Won
		prochainNiveau();
		//game->setCurrentGameScreen(HighScore);
	}

    if(!game->martienArmy->update(elapsedTicks))
    {
		gameover();
		return false;
		//gameover
    }

	//update la position du vaiseau
    if(!game->ship->update(elapsedTicks,game->engine))
	{
        game->engine->Audio->jouerWaveChannel2(L".\\Sounds\\explosion.wav");
        RECTANGLE rect = game->ship->getRect();
        game->explosions.push_back(new ExplosionMartien(rect.left+rect.width/2,rect.top+rect.height/2,500));

        if(game->nbLives == 0)
        {
            //dead
            gameover();
        }

        game->nbLives--;

	    while(game->laserMartiens.size() !=0)
	    {
		    delete game->laserMartiens[game->laserMartiens.size()-1];
		    game->laserMartiens.pop_back();
	    }

        return true;
	}

	//update les power up
	vector<PowerUp*>::iterator iterPowerUp = game->powerUps.begin();
	while (iterPowerUp != game->powerUps.end())
	{
        if(!(*iterPowerUp)->update(elapsedTicks,game->engine))
		{   
			delete (*iterPowerUp);
			iterPowerUp = game->powerUps.erase(iterPowerUp);
		}
		else
		{
			iterPowerUp++;
		}
	}

	return true;
}

bool GamePlayScreen::render()
{
    game->ship->render(game->engine);

    game->martienArmy->render(game->engine);    

	for(vector<Explosion*>::iterator it = game->explosions.begin();it != game->explosions.end();++it)
	{
		(*it)->render(game->engine);
	}

	for(vector<LaserMartien*>::iterator it = game->laserMartiens.begin();it != game->laserMartiens.end();++it)
	{
        (*it)->render(game->engine);
	}

	for(vector<PowerUp*>::iterator it = game->powerUps.begin();it != game->powerUps.end();++it)
	{
        (*it)->render(game->engine);
	}

    afficherInfo();

    return true;
}


bool GamePlayScreen::key_pressed(int elapsedTick,int virtualKey)
{
	//game->ship->key_pressed(elapsedTick,virtualKey);
	return true;
}

bool GamePlayScreen::key_released(int elapsedTick,int virtualKey)
{
    switch(virtualKey)
    {
        case KEY_ESCAPE:
            game->setCurrentGameScreen(Pause);
            break;
    }

	return true;
}

void GamePlayScreen::afficherInfo()
{
    wstringstream output;
    output << L"Score: " << setfill (L'0') << setw (5) << game->getScore() << " Lives: " << setfill (L' ') << setw (5) << wstring(game->nbLives,0x1) << " Shields: " << setw (5) << wstring(game->ship->getShields(),'#') << "Lasers: " << setw (5) << wstring(game->ship->getMaxLaser() - game->ship->getNbLaser(),L'↑') << " Niveau: " << game->getNiveau();
    game->engine->Display->writeLine(0,0,game->engine->Display->ConsoleWidth(),0,Utility::makeColor(White,Blue));
    game->engine->Display->writeLine(0,1,game->engine->Display->ConsoleWidth(),1,Utility::makeColor(White,Blue));
    game->engine->Display->writeLine(0,2,game->engine->Display->ConsoleWidth(),2,Utility::makeColor(White,Blue));
    game->engine->Display->writeText(2,1,Utility::makeColor(White,Black),output.str());    
}