﻿#include "IntroScreen.h"
#include <sstream>


IntroScreen::IntroScreen(GameEngine *game)
	: GameScreen(game)
{
}

IntroScreen::~IntroScreen(void)
{
}

bool IntroScreen::initialise()
{
	wstring space[] = { L" #####                             ",
						L"#     # #####    ##    ####  ######",
						L"#       #    #  #  #  #    # #     ",
						L" #####  #    # #    # #      ##### ",
						L"      # #####  ###### #      #     ",
						L"#     # #      #    # #    # #     ",
						L" #####  #      #    #  ####  ######"};

	wstring invaders[] = {  L"###                                                   #########",
							L" #  #    # #    #   ##   #####  ###### #####   ####     ## ##  ",
							L" #  ##   # #    #  #  #  #    # #      #    # #         ## ##  ",
							L" #  # #  # #    # #    # #    # #####  #    #  ####     ## ##  ",
							L" #  #  # # #    # ###### #    # #      #####       #    ## ##  ",
							L" #  #   ##  #  #  #    # #    # #      #   #  #    #    ## ##  ",
							L"### #    #   ##   #    # #####  ###### #    #  ####   #########"};

	wstring bombarider[] = {	L"/___\\",
								L" / \\ "};

	wstring mitrailleur[] = { L" /^\\ ",
							  L" ||| "};

	wstring officier[] = { L"/~~~\\",
						   L" /V\\ "};

	wstringstream shield; 
	shield << L"▀S▀";

	wstringstream laser; 
	laser << L"▀L▀";

	spaceText = new TextBlock(space,7,7);
	invadersText = new TextBlock(invaders,7,7);
	bombariderText = new TextBlock(bombarider,2,Utility::makeColor(Purple,Black));
	mitrailleurText = new TextBlock(mitrailleur,2,Utility::makeColor(Cyan,Black));
	officierText = new TextBlock(officier,2,Utility::makeColor(Green,Black));
	shieldText = new TextBlock(shield.str(),Utility::makeColor(LightBlue,Black));
	laserText = new TextBlock(laser.str(),Utility::makeColor(Yellow,Black));

	return true;
}

bool IntroScreen::dispose()
{
	delete spaceText;
	delete invadersText;
	delete bombariderText;
	delete mitrailleurText;
	delete officierText;
	delete shieldText;
	delete laserText;
	return true;
}

bool IntroScreen::update(int elapsedTick)
{
	return true;
}

bool IntroScreen::render()
{
	game->engine->Display->displayTextBlock(5,5,spaceText);
	game->engine->Display->displayTextBlock(30,13,invadersText);
	game->engine->Display->writeText(40,22,7,L"* Score table *");
	game->engine->Display->displayTextBlock(30,25,mitrailleurText);
	game->engine->Display->writeText(35,25,7,L"  MITRAILLEUR    = 30 POINTS");
	game->engine->Display->displayTextBlock(30,30,officierText);
	game->engine->Display->writeText(35,30,7,L"  OFFICIER       = 20 POINTS");
	game->engine->Display->displayTextBlock(30,35,bombariderText);
	game->engine->Display->writeText(35,35,7,L"  BOMBARDIER     = 10 POINTS");
	game->engine->Display->displayTextBlock(31,40,shieldText);
	game->engine->Display->writeText(35,40,7,L"  POWERUP SHIELD = 10 POINTS");
	game->engine->Display->displayTextBlock(31,45,laserText);
	game->engine->Display->writeText(35,45,7,L"  POWERUP LASER  = 10 POINTS");
	
	return true;
}

bool IntroScreen::key_pressed(int elapsedTick,int virtualKey)
{
	return true;
}

bool IntroScreen::key_released(int elapsedTick,int virtualKey)
{    
	game->setCurrentGameScreen(GamePlay);
	return true;
}
