#pragma once
#include "../GameEngine.h"
#include "GameScreen.h"
#include "../MenuGen.h"

class PauseScreen :
	public GameScreen
{
public:
	PauseScreen(GameEngine *game);
	~PauseScreen(void);

	MenuGen *menuPause;
		
    bool initialise();
    bool dispose();
    bool update(int elapsedTick);
    bool render();
	bool key_pressed(int elapsedTick,int virtualKey);
	bool key_released(int elapsedTick,int virtualKey);
};

