#include "PauseScreen.h"

PauseScreen::PauseScreen(GameEngine *game)
	: GameScreen(game)
{
    menuPause = NULL;
}

PauseScreen::~PauseScreen(void)
{    
}

bool PauseScreen::initialise()
{
	menuPause = new MenuGen(60, 35);
	menuPause->addOption(L"Continuer partie");
	menuPause->addOption(L"Reprendre partie");
	menuPause->addOption(L"Main Menu");
	return true;
}

bool PauseScreen::dispose()
{
    if(menuPause != NULL)
        delete menuPause;

	return true;
}

bool PauseScreen::update(int elapsedTick)
{
	return true;
}

bool PauseScreen::render()
{
	menuPause->render(game->engine);
	return true;
}

bool PauseScreen::key_pressed(int elapsedTick,int virtualKey)
{ 
    return true;
}

bool PauseScreen::key_released(int elapsedTick,int virtualKey)
{
	menuPause->key_released(elapsedTick,virtualKey);

    switch(virtualKey)
    {
    case KEY_RETURN_KEY:
        {
            int selection = menuPause->GetCurrentSelection();
            switch(selection)
            {
            case 0: // Continuer partie
                {
                    game->setCurrentGameScreen(GamePlay);
                }
                break;
            case 1: // Reprendre partie
                {
                    game->setCurrentGameScreen(GamePlay);					 
                }
                break;
            case 2: // Main Menu
                {
                    game->setCurrentGameScreen(MainMenu);
                }
            }
        }
        break;
    }

	return true;
}