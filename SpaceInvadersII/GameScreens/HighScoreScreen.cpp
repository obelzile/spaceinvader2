#include "HighScoreScreen.h"
#include <sstream>
#include <iomanip>
using namespace std;

HighScoreScreen::HighScoreScreen(GameEngine *game)
    : GameScreen(game)
{
    input = NULL;
}


HighScoreScreen::~HighScoreScreen(void)
{
}


bool HighScoreScreen::initialise()
{
    wstring space[] = { L" #####                             ",
                        L"#     # #####    ##    ####  ######",
                        L"#       #    #  #  #  #    # #     ",
                        L" #####  #    # #    # #      ##### ",
                        L"      # #####  ###### #      #     ",
                        L"#     # #      #    # #    # #     ",
                        L" #####  #      #    #  ####  ######"};
    
    wstring invaders[] = {  L"###                                                   #########",
                            L" #  #    # #    #   ##   #####  ###### #####   ####     ## ##  ",
                            L" #  ##   # #    #  #  #  #    # #      #    # #         ## ##  ",
                            L" #  # #  # #    # #    # #    # #####  #    #  ####     ## ##  ",
                            L" #  #  # # #    # ###### #    # #      #####       #    ## ##  ",
                            L" #  #   ##  #  #  #    # #    # #      #   #  #    #    ## ##  ",
                            L"### #    #   ##   #    # #####  ###### #    #  ####   #########"};       

    spaceText = new TextBlock(space,7,7);
    invadersText = new TextBlock(invaders,7,7);

    nouveauHighScore = 0;
    inputPlayer = false;

    if(scores.estNouveauHighScore(game->score))
    {
        nouveauHighScore = game->score;
        game->score = 0;
        inputPlayer = true;
        input = new TextInput(3,game->engine->Display->getCenter().getPositionX()-5,22);
    }    

    return true;
}

bool HighScoreScreen::dispose()
{
    
    return true;
}

bool HighScoreScreen::update(int elapsedTick)
{
    if(input != NULL)
        input->update(elapsedTick);

    if(input != NULL && !input->isActive() && inputPlayer)
    {
        inputPlayer = false;
        wstring nom = input->getInput();
        scores.ajouterScore(nom,nouveauHighScore);
        delete input;
        input = NULL;
    }

    return true;
}

bool HighScoreScreen::render()
{
    int centerX = game->engine->Display->getCenter().getPositionX();

    if(input != NULL)
        input->render();

    game->engine->Display->displayTextBlock(5,5,spaceText);
    game->engine->Display->displayTextBlock(30,13,invadersText);

    if(nouveauHighScore > 0)
    {
        wstringstream output;
        output << L"Nouveau HighScores " << setfill (L'0') << setw (5) << nouveauHighScore;
        game->engine->Display->writeText(centerX-5,26,Utility::makeColor(White,Black),output.str());
    }
    else
    {
        game->engine->Display->writeText(centerX-5,26,Utility::makeColor(White,Black),L"HighScores");
    }

    for(int i=0;i<MAX_SCORES;i++)
    {
        wstringstream output;
        output << i+1 << L"- " << setfill (L'0') << setw (5) << scores.getScoreAtIndex(i) << " " << scores.getNameAtIndex(i);
        game->engine->Display->writeText(centerX-5,28+i*2,Utility::makeColor(White,Black),output.str());
    }

    return true;
}


bool HighScoreScreen::key_pressed(int elapsedTick,int virtualKey)
{
    if(input != NULL)
        input->key_pressed(elapsedTick,virtualKey);

	return true;
}

bool HighScoreScreen::key_released(int elapsedTick,int virtualKey)
{
    if(input == NULL || (input != NULL && !input->isActive()))
        game->setCurrentGameScreen(MainMenu);

    if(input != NULL)
        input->key_released(elapsedTick,virtualKey);

	return true;
}