#pragma warning (disable : 4482)

#include "StartScreen.h"
#include <string>
using namespace std;

#include "MainMenuScreen.h"

StartScreen::StartScreen(GameEngine *game)
    : GameScreen(game)
{
	spaceTextBlink = NULL;
	invadersTextBlink = NULL;
	startTextBlink = NULL;
    spaceText = NULL;
    invadersText = NULL;
}

StartScreen::~StartScreen(void)
{
	game->engine->Audio->arreterMP3Channel2();

	if(spaceTextBlink == NULL)
		delete spaceTextBlink;
	if(invadersTextBlink == NULL)
		delete invadersTextBlink;
	if(startTextBlink == NULL)
		delete startTextBlink;
	if(spaceText != NULL)
		delete spaceText;
	if(invadersText != NULL)
		delete invadersText;
}

bool StartScreen::initialise()
{
    wstring space[] = { L" #####                             ",
                        L"#     # #####    ##    ####  ######",
                        L"#       #    #  #  #  #    # #     ",
                        L" #####  #    # #    # #      ##### ",
                        L"      # #####  ###### #      #     ",
                        L"#     # #      #    # #    # #     ",
                        L" #####  #      #    #  ####  ######"};
    
    wstring invaders[] = {  L"###                                                   #########",
                            L" #  #    # #    #   ##   #####  ###### #####   ####     ## ##  ",
                            L" #  ##   # #    #  #  #  #    # #      #    # #         ## ##  ",
                            L" #  # #  # #    # #    # #    # #####  #    #  ####     ## ##  ",
                            L" #  #  # # #    # ###### #    # #      #####       #    ## ##  ",
                            L" #  #   ##  #  #  #    # #    # #      #   #  #    #    ## ##  ",
                            L"### #    #   ##   #    # #####  ###### #    #  ####   #########"};

    wstring start = L"PRESS START";

    spaceTextBlink = new BlinkingText(space,7,7,180);
    invadersTextBlink = new BlinkingText(invaders,7,7,180);
    startTextBlink = new BlinkingText(start,7,180);

    game->engine->Audio->arreterMP3Channel2();
    
    game->engine->Audio->jouerMP3Channel2(L".\\Sounds\\Opening.mp3",0);
    
    return true;
}

bool StartScreen::dispose()
{
    game->engine->Audio->arreterMP3Channel2();

	if(spaceTextBlink == NULL)
		delete spaceTextBlink;
	if(invadersTextBlink == NULL)
		delete invadersTextBlink;
	if(startTextBlink == NULL)
		delete startTextBlink;
	if(spaceText != NULL)
		delete spaceText;
	if(invadersText != NULL)
		delete invadersText;

    return true;
}

bool StartScreen::update(int elapsedTick)
{
    spaceTextBlink->update(elapsedTick,game->engine);
    invadersTextBlink->update(elapsedTick,game->engine);
    startTextBlink->update(elapsedTick,game->engine);
    
    int position = game->engine->Audio->getCurrentMP3Millisecond();
    if(position >= 24600)
    {
        game->setCurrentGameScreen(GameScreenEnum::MainMenu);

        return false;
    }

    return true;
}

bool StartScreen::render()
{
    game->engine->Display->displayTextBlock(5,5,spaceTextBlink);
    game->engine->Display->displayTextBlock(30,13,invadersTextBlink);
    game->engine->Display->displayTextBlock(game->engine->Display->getCenter().getPositionX()-startTextBlink->getBlockWidth()/2,25,startTextBlink);

    return true;
}

bool StartScreen::key_pressed(int elapsedTick,int virtualKey)
{
	
	return true;

}

bool StartScreen::key_released(int elapsedTick,int virtualKey)
{
	game->setCurrentGameScreen(GameScreenEnum::MainMenu);
	return true;
}