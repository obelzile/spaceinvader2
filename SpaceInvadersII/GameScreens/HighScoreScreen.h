#pragma once

#include "../GameEngine.h"
#include "GameScreen.h"
#include "../HighScore.h"
#include "../TextInput.h"
class HighScoreScreen :
    public GameScreen
{
private:
    TextBlock *spaceText;
    TextBlock *invadersText;
    HighScores scores;
    TextInput *input;
    int nouveauHighScore;
    bool inputPlayer;

public:
    HighScoreScreen(GameEngine *game);
    ~HighScoreScreen(void);

    bool initialise();
    bool dispose();
    bool update(int elapsedTick);
    bool render();
	bool key_pressed(int elapsedTick,int virtualKey);
	bool key_released(int elapsedTick,int virtualKey);
};

