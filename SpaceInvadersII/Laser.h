#pragma once

#include "GameObject.h"

class Laser :
    public GameObject
{
	public:
		void startLaser(int);
		virtual void moveLaser();
};