#include <iostream>
#include <string>
#include <fstream>
#include "HighScore.h"

using namespace std;

HighScores::HighScores(void)
{
    for(int i=0;i<MAX_SCORES;i++)
    {
        noms[i]=L"MAR";
        scores[i]=i*10;
    }
    lire();
}

void HighScores::ecrire()
{
    // ouverture en �criture avec effacement du fichier ouvert
	wofstream fichier("highscore.txt", ios::out | ios::trunc);  

	if(fichier)
	{
		for(int i=0; i<MAX_SCORES ; i++)
		{
            /*on lit jusqu'� l'espace et on stocke ce qui est lu dans la variable indiqu�e */
			fichier << noms[i] << " " << scores[i] << endl;
		}
		fichier.close(); 
	}
}

void HighScores::lire()
{
	wifstream fichier("highscore.txt", ios::in);

	if(fichier)
	{
		for(int i=0; i<MAX_SCORES ; i++)
		{
            /*on lit jusqu'� l'espace et on stocke ce qui est lu dans la variable indiqu�e */
			fichier >> noms[i] >> scores[i];  
		}
		fichier.close();		
	}
}

bool HighScores::estNouveauHighScore(int score)
{
	int i=0;
	while(scores[i]>score)
		i++;

	if(i<MAX_SCORES)
	{
        return true;
    }
    return false;
}

void HighScores::ajouterScore(wstring nom, int score)
{
	int i=0;
	while(scores[i] > score)
		i++;

	if(i < MAX_SCORES)
	{
		//	d�caler le tableau vers la droite a partir de l'indice i
		for(int j=3;j>=i;j--)
		{
			noms[j+1]=noms[j];
			scores[j+1]=scores[j];
		}
		//	inserer le nouveau score
		noms[i]=nom;
		scores[i]=score;

        ecrire();
	}
}