#include "MenuGen.h"
#include <iostream>

MenuGen::MenuGen(int positionX,int positionY)
	: 	currentSelection(0)
{
    coord.setPositionX(positionX);
    coord.setPositionY(positionY);

	currentTicksCount = 0;
	delay=0;
}

MenuGen::~MenuGen(void)
{
}

bool MenuGen::initialise()
{
	return true;
}

int MenuGen::getWidth()
{
    unsigned int maxWidth = 0;

	for (unsigned int i=0; i< options.size(); i++)
	{
        if(options[i].length() > maxWidth)
            maxWidth = options[i].length();
	}

    return maxWidth;
}

bool MenuGen::addOption(wstring option)
{
	options.push_back(option);// insere une option au vecteur d'option(tableau)
	return true;// retourne valeur bool true
}

bool MenuGen::key_pressed(int elaspeTicks, int virtualKey)
{
	return true;	

}

bool MenuGen::key_released(int elaspeTicks, int virtualKey)
{
	switch (virtualKey)
	{
	case KEY_UP: // haut
			{
				if (currentSelection == 0)
					currentSelection  = options.size()-1;
				else currentSelection --;
			}						
			break;
	case KEY_DOWN: //bas
		{
			if (currentSelection == options.size()-1)
				currentSelection = 0;
			else currentSelection ++;
		}
        break;
	}
	return true;// validation
}

bool MenuGen::update(int elaspeTicks)
{
	return true;
}

bool MenuGen::render(ConsoleEngine *engine)
{
	for (unsigned int i=0; i< options.size(); i++)
	{
		if (currentSelection != i)
            engine->Display->writeText(coord.getPositionX(),coord.getPositionY()+i*2,Utility::makeColor(White,Black),options[i]);
		else
			engine->Display->writeText(coord.getPositionX()-2,coord.getPositionY()+i*2,Utility::makeColor(Black,White),L"- " + options[i]+ L" -");		
	}
	return true;
}

const wstring MenuGen::getOption(int index) const
{
	if(index < 0 || index > (int)options.size()-1)
		return L"";
	else
		return options[index];
}