#pragma once
#include <vector>
using namespace std;

#include "MartienArmy.h"
#include "ConsoleEngine\ConsoleEngine.h"
#include "Explosion.h"
#include "LaserMartien.h"
#include "SpaceShip.h"
#include "PowerUp.h"

#include "GameScreens\GameScreen.h"
#include "GameScreens\StartScreen.h"
#include "GameScreens\MainMenuScreen.h"
#include "GameScreens\HighScoreScreen.h"
#include "GameScreens\GamePlayScreen.h"
#include "GameScreens\PauseScreen.h"
#include "GameScreens\IntroScreen.h"

class SpaceShip;
class LaserMartien;
class StartScreen;
class MainMenuScreen;
class HighScoreScreen;
class GamePlayScreen;
class IntroScreen;
class PauseScreen;
class MartienArmy;

enum GameScreenEnum
{
	Quit,
	Start,
	MainMenu,
	Intro,
	HighScore,
	GamePlay,
    Pause
};

class GameEngine
{
public:	
	SpaceShip *ship;
	vector<Explosion *> explosions;
	vector<LaserMartien *> laserMartiens;
    vector<PowerUp *> powerUps;
    MartienArmy *martienArmy;

	GameScreen *currentScreen;
	StartScreen *startScreen;
	MainMenuScreen *mainMenuScreen;
	HighScoreScreen *highScoreScreen;
	GamePlayScreen *gameplayScreen;
    PauseScreen *pauseScreen;
	IntroScreen *introScreen;

	int score;
	int nbLives;
	int niveau;

	int currentTicks;
	int previousTicks;  

public:
	ConsoleEngine* engine;

	GameEngine(void);
	~GameEngine(void);
	bool initialise();
	bool dispose();

	bool setCurrentGameScreen(GameScreenEnum screen);
    void ajouterScore(int value) { this->score += value; }
	int getNiveau() { return this->niveau; }
	void setNiveau(int value) { this->niveau = value;}
    int getScore() { return this->score; }
	bool run();
	bool update(int elapsedTick);
	bool render();
	bool key_pressed(int elapsedTick,int virtualKey);
	bool key_released(int elapsedTick,int virtualKey);
};

