#pragma once

#include <vector>
using namespace std;

#include "GameObject.h"

#include "ConsoleEngine\TextBlock.h"
#include "ShipLaser.h"
#include "GameEngine.h"

class GameEngine;
class ShipLaser;

class SpaceShip :
    public GameObject
{
private:
	GameEngine *game;
    TextBlock *spaceship;
    bool shipDead;
    int maxLaser;
    int maxShields;
    int shields;
    int moveDelay;
    int fireDelay;
    int moveTickCount;
    int fireTickCount;
    int shipWidth;
    int shipHeight;
    vector<ShipLaser*> activeLasers;
    
public:
    SpaceShip(GameEngine *game);
    ~SpaceShip(void);

    vector<ShipLaser*>* getActiveLaser() { return &activeLasers; };
	RECTANGLE getRect();

    bool getShipDead() { return shipDead; }
    void setShipDead(bool value) { shipDead=value; }
    void setMaxLaser(int value);
    void setShields(int value);
    int getMaxLaser() { return maxLaser; }
    int getShields() { return shields; }
    int getMaxShields() { return maxShields; }
    int getNbLaser() { return activeLasers.size(); }
    
    void deplacerGauche();
    void deplacerDroite();
    void fireLaser();
	bool testCollision();
    bool collisionTest(int positionX,int positionY);
    
    bool update(int elapsedTicks,ConsoleEngine *engine);
	bool render(ConsoleEngine *engine);
};

