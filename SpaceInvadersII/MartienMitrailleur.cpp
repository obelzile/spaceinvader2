﻿#include <string>
using namespace std;
#include "MartienMitrailleur.h"

MartienMitrailleur::MartienMitrailleur(int positionX,int positionY,int fireDelay,int type,int valeur,GameEngine *game) : Martien(fireDelay,type,valeur,game)
{
	coord.setPositionX(positionX);
	coord.setPositionY(positionY);

	wstring frame1[] = { L" /^\\ ",
						 L" ||| "};

	wstring frame2[] = { L" /^\\ ",
						 L"|^|^|"};

	frames = new AnimationBlock(coord.getPositionX(),coord.getPositionY(),500,true);
	frames->addFrame(frame1,2,Utility::makeColor(Cyan,Black));
	frames->addFrame(frame2,2,Utility::makeColor(Cyan,Black));
}


MartienMitrailleur::~MartienMitrailleur(void)
{
}

bool MartienMitrailleur::collisionTest(int positionX,int positionY)
{
    if(positionX >= frames->getPositionX() && positionX <= frames->getPositionX() + frames->getWidth()
        && positionY >= frames->getPositionY() && positionY <= frames->getPositionY() + frames->getHeight())
        return true;

    return false;
}

bool MartienMitrailleur::canFire(int elapsedTicks)
{
    bool canFire = false;

	currentFireDelay +=elapsedTicks;
	if(currentFireDelay >= fireDelay)
	{
        canFire = true;
		currentFireDelay = 0;

        //ne pas tirer si un martien est present en dessous
	    for(vector<Martien*>::iterator iterAlien = game->martienArmy->martiens.begin();iterAlien != game->martienArmy->martiens.end();++iterAlien)
	    {           		
            if((*iterAlien)->coord.getPositionX() == this->coord.getPositionX() && (*iterAlien)->coord.getPositionY() > this->coord.getPositionY())
            {
                canFire = false;
            }
	    }

		return canFire;
	}

	return canFire;
}

bool MartienMitrailleur::fireLaser()
{
    if(game->laserMartiens.size() < 10)
    {
        game->laserMartiens.push_back(new LaserMartien(coord.getPositionX()+2,coord.getPositionY()+2,L"↓",Utility::makeColor(Green,Black),50));
        return true;
    }

    return false;
}

bool MartienMitrailleur::update(int elapsedTicks, ConsoleEngine *engine)
{
	//frames->update(elapsedTick,game->engine);
	return true;
}

bool MartienMitrailleur::render(ConsoleEngine *engine)
{    
    game->engine->Display->displayTextBlock(coord.getPositionX(),coord.getPositionY(),frames->getCurrentFrame());
	return true;
}