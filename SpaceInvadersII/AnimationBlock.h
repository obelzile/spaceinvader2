#pragma once

#include <vector>
using namespace std;
#include "consoleengine\textblock.h"
#include "ConsoleEngine\ConsoleEngine.h"

class AnimationBlock
{
private:
	vector<TextBlock*> blockFrames;
	bool loopFrame;
	bool blinkEnabled;
	int currentFrame;
	int frameChangeSpeed;
	int currentTicksCount;
	int positionX;
	int positionY;

public:
	AnimationBlock(int positionX,int positionY,int frameChangeSpeed,bool loop);
	~AnimationBlock(void);
    const int getPositionX() { return positionX; }
    const int getPositionY() { return positionY; }
    void setPositionX(int value) { positionX = value; }
    void setPositionY(int value) { positionY = value; }

    const int getWidth() { return blockFrames[currentFrame]->getBlockWidth(); }
    const int getHeight() { return blockFrames[currentFrame]->getBlockHeight(); }
	const bool enabled() const { return blinkEnabled; }
    bool nextFrame();
    const int getNbFrames() { return blockFrames.size(); }
    TextBlock* getCurrentFrame() { return blockFrames[currentFrame]; }
	void enabled(bool value) { blinkEnabled = value; }
	const int frameSpeed() const {return frameChangeSpeed; }
	void frameSpeed(int value) { frameChangeSpeed = value; }
	void addFrame(TextBlock *frame);
    void addFrame(wstring frame,int color);
	void addFrame(wstring frame[],int nbLines,int color);
	bool update(int elapsedTick,ConsoleEngine *engine);
	bool render(ConsoleEngine *engine);
};

