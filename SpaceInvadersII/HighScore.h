#pragma once

#include <string>
using namespace std;

#define MAX_SCORES 5

class HighScores
{
private:
	wstring noms[MAX_SCORES];
	int scores[MAX_SCORES];

public:
	HighScores(void);
    wstring getNameAtIndex(int index) { return noms[index]; }
    int getScoreAtIndex(int index) { return scores[index]; }
	void ecrire();
	void lire();
    bool estNouveauHighScore(int score);
	void ajouterScore(wstring nom, int score);
};

