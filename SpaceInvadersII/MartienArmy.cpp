﻿#include "MartienArmy.h"
#include "MartienMitrailleur.h"
#include "MartienBombardier.h"
#include "MartienOfficier.h"

MartienArmy::MartienArmy(GameEngine *game) : game(game)
{
    currentTicksCount = 0;
    currentSoundTicks = 0;
    lastSoundTicks = 0;
    soundStep = 0;
    direction = false;
    speed = 600;   
}

MartienArmy::~MartienArmy(void)
{
}

bool MartienArmy::initialise()
{    
    for(int i=0;i<11;i++)
    {
        martiens.push_back(new MartienMitrailleur(5+(7*i),5,100,0,30,game));        
    }

    for(int i=0;i<11;i++)
    {
        martiens.push_back(new MartienOfficier(5+(7*i),10,100,0,20,game));        
    }

    for(int i=0;i<11;i++)
    {
        martiens.push_back(new MartienOfficier(5+(7*i),15,100,0,20,game));        
    }

    for(int i=0;i<11;i++)
    {
        martiens.push_back(new MartienBombardier(5+(7*i),20,100,0,10,game));        
    }

    for(int i=0;i<11;i++)
    {
        martiens.push_back(new MartienBombardier(5+(7*i),25,100,0,10,game));        
    }

    return true;
}

bool MartienArmy::dispose()
{
    return true;
}

bool MartienArmy::moveArmyRight()
{
    Martien* rightMost = martiens.front();
    bool succeed = true;

    for(vector<Martien*>::iterator iter = martiens.begin();iter != martiens.end();++iter)
	{           
        if((*iter)->coord.getPositionX() > rightMost->coord.getPositionX())
            rightMost = (*iter);
	}

    RECTANGLE rect = rightMost->getRect();
    if(rect.left + rect.width < game->engine->Display->ConsoleWidth()-4)
    {
        for(vector<Martien*>::iterator iter = martiens.begin();iter != martiens.end();++iter)
	    {           
            (*iter)->moveXofOffset(1);
	    }
    }
    else
    {
        succeed = false;
    }
    return succeed;
}

bool MartienArmy::moveArmyLeft()
{    
    Martien* leftMost = martiens.front();
    bool succeed = true;

    for(vector<Martien*>::iterator iter = martiens.begin();iter != martiens.end();++iter)
	{           
        if((*iter)->coord.getPositionX() < leftMost->coord.getPositionX())
            leftMost = (*iter);
	}

    RECTANGLE rect = leftMost->getRect();
    if(rect.left > 3)
    {
        for(vector<Martien*>::iterator iter = martiens.begin();iter != martiens.end();++iter)
	    {           
            (*iter)->moveXofOffset(-1);
	    }
    }
    else
    {
        succeed = false;
    }

    return succeed;
}

bool MartienArmy::moveArmyDown()
{
    Martien* downMost = martiens.front();
    bool succeed = true;

    for(vector<Martien*>::iterator iter = martiens.begin();iter != martiens.end();++iter)
	{           
        if((*iter)->coord.getPositionY() > downMost->coord.getPositionY())
            downMost = (*iter);
	}

    RECTANGLE rect = downMost->getRect();
    if(rect.top + rect.height < game->engine->Display->ConsoleHeight()-5)
    {
        for(vector<Martien*>::iterator iter = martiens.begin();iter != martiens.end();++iter)
	    {           
            (*iter)->moveYofOffset(3);
	    }
    }
    else
    {
        succeed = false;
    }

    return succeed;
}


bool MartienArmy::update(int elapsedTicks)
{
    currentTicksCount +=elapsedTicks;
    bool succeed = true;

    if(martiens.size() == 0)
        return false;

    if(currentTicksCount >= speed)
    {
        currentTicksCount = 0;
        
        if(!direction) // right
        {
            succeed = moveArmyRight();
        }
        else // left
        {
            succeed = moveArmyLeft();
        }
		
        if(!succeed) // down
        {
            direction = !direction;
            succeed = moveArmyDown();
        }

        for(vector<Martien*>::iterator it = martiens.begin();it != martiens.end();++it)
	    {           
            (*it)->update(elapsedTicks,game->engine);
	    }
    }  
    moveSound(elapsedTicks);
    
	//les martiens attaque
	for(vector<Martien*>::iterator iterAlien = martiens.begin();iterAlien != martiens.end();++iterAlien)
	{           		
		if((*iterAlien)->canFire(elapsedTicks))
		{
			if(rand()%100 < 10)
			{	
                (*iterAlien)->fireLaser();
			}
		}
	}

    return succeed;
}

void MartienArmy::updateSpeed()
{
    speed-=10;
}

bool MartienArmy::moveSound(int elapsedTick)
{    
    currentSoundTicks += elapsedTick;
    if(currentSoundTicks >= speed)
    {
        currentSoundTicks = 0;
        switch(soundStep)
        {
        case 0:
            game->engine->Audio->arreterWaveChannel1();
            game->engine->Audio->jouerWaveChannel1(L".\\Sounds\\fastinvader4.wav",SND_ASYNC | SND_NOSTOP);
            break;
        case 1:
            game->engine->Audio->arreterWaveChannel1();
            game->engine->Audio->jouerWaveChannel1(L".\\Sounds\\fastinvader1.wav",SND_ASYNC | SND_NOSTOP);
            break;
        case 2:
            game->engine->Audio->arreterWaveChannel1();
            game->engine->Audio->jouerWaveChannel1(L".\\Sounds\\fastinvader2.wav",SND_ASYNC | SND_NOSTOP);
            break;
        case 3:
            game->engine->Audio->arreterWaveChannel1();
            game->engine->Audio->jouerWaveChannel1(L".\\Sounds\\fastinvader3.wav",SND_ASYNC | SND_NOSTOP);
            break;
        }
        soundStep++;
        if(soundStep>3)
            soundStep=0;
    }

    return true;
}

bool MartienArmy::render(ConsoleEngine *engine)
{
    for(vector<Martien*>::iterator it = martiens.begin();it != martiens.end();++it)
	{           
        (*it)->render(engine);
	}

    return true;
}