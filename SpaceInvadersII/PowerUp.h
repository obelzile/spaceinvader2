#pragma once

#include "AnimationBlock.h"
#include "GameObject.h"

class GameEngine;

enum PowerUpType
{
    Shield,
    Laser
};

class PowerUp : 
    public GameObject
{
private:
    PowerUpType type;
    int fallSpeed;
    AnimationBlock* frames;
    int currentTicksCount;    

public:

    PowerUp(int positionX,int positionY,PowerUpType type, int fallSpeed);
    ~PowerUp(void);

    PowerUpType getType() { return type; }
    bool initialise();
    bool dispose();

    bool update(int elapsedTicks, ConsoleEngine *engine);
	bool render(ConsoleEngine *engine);
};

