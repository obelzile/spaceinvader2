#include "Utility.h"
#include <Windows.h>


int Utility::intervalAleatoire(int min,int max)
/*
    T�che: generer un nombre aleatoire entre min max
    Param�tres: min valeur minimum du random
                max valeur maximum du random
    Retour: la valeur aleatoire
*/
{
    if(Utility::initialiser == false)
    {
        initialiser = true;
        srand((unsigned int)time(NULL));
    }

	return min + rand() % (max - min + 1);
}

string Utility::charToOem(const wstring &texte)
{
	CHAR* c = new CHAR[texte.size()+1]; // ASCII OEM une string 
	CharToOem(texte.c_str(),c); 
	std::string t = c;
	string temp = string(c);
	delete [] c;
	return temp;
}

int Utility::getCurrentTick()
{
    return clock()/(CLOCKS_PER_SEC / 1000);
}

int Utility::makeColor(Color fgColor,Color bgColor)
{
    return fgColor | bgColor << 4;
}