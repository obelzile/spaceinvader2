#pragma once

#include <Windows.h>
#include <string>
using namespace std;
#include "../Coord.h"
#include "Utility.h"
#include "TextBlock.h"

class DisplayEngine
{
private:
    int consoleWidth;
	int consoleHeight;
	HANDLE hOutput;
    CHAR_INFO *buffer; 
    CHAR_INFO *bufferblank;
    COORD dwBufferSize;
    COORD dwBufferCoord;
    SMALL_RECT rcRegion;
	bool canDraw;
    int frameCount;

public:
	//Constructor
	DisplayEngine(int width,int height);
    //Destructor
    ~DisplayEngine(void);

	//Properties
	int ConsoleWidth() const { return consoleWidth; }
	//void ConsoleWidth(int val) { consoleWidth = val; }
	int ConsoleHeight() const { return consoleHeight; }
	//void ConsoleHeight(int val) { consoleHeight = val; }
	bool CanDraw() const { return canDraw; }
	void CanDraw(bool val) { canDraw = val; }

    //Methods
    Coord getCenter();
	BOOL WINAPI SetConsoleIcon(HICON hIcon);
	static BOOL WINAPI ConsoleHandlerRoutine(DWORD dwCtrlType);
    void clearBuffer(int color = 0);
    void writeLine(int x1,int y1,int x2 ,int y2,int color);
    void writeText(int x,int y,int color, const wstring &text);
    void writeText(int x,int y,int color, const wchar_t character);
    void displayTextBlock(int positionX ,int positionY ,TextBlock *textBlock);
    void displayObject(int x,int y, const CHAR_INFO *objectBlock,int width,int height);
    bool draw();
    void curseurVisible(bool visibilite);
};

