#pragma once

class ICallbackInput
{
public:
    virtual bool key_pressed(int elapsedTick,int virtualKey) = 0;
	virtual bool key_released(int elapsedTick,int virtualKey) = 0;
};
