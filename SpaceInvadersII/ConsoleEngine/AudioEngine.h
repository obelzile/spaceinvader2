#pragma once

#include <windows.h>
#include <mmsystem.h>
#include <stdio.h>

#pragma comment( lib, "winmm.lib" )

class AudioEngine
{
public:
    AudioEngine(void);
    ~AudioEngine(void);

    void jouerWaveChannel1(LPCWSTR fichier,int flags);
    void arreterWaveChannel1();
    void jouerMP3Channel2(LPCWSTR fichier,int startFrom=0);
    void arreterMP3Channel2();
    int getCurrentMP3Millisecond();
    void jouerWaveChannel2(LPCWSTR fichier);
    void arreterWaveChannel2();
};

