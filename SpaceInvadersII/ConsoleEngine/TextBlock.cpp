#include "TextBlock.h"


TextBlock::TextBlock(wstring textblock[],int blockLines,int color)
{
    textBlock = NULL;
    blockWidth = 0;
    blockHeight = blockLines;

    //Find the largest width
    for(int i=0;i<blockHeight;i++)
    {
        if((int)textblock[i].length() > blockWidth)
            blockWidth = textblock[i].length();
    }

    textBlock = new CHAR_INFO[blockWidth*blockHeight];

    for(int line=0;line<blockHeight;line++)
    {
        for(unsigned int index=0;index<textblock[line].length();index++)
        {
            textBlock[line * blockWidth + index].Attributes = color;
            textBlock[line * blockWidth + index].Char.UnicodeChar = textblock[line][index];
        }
    }
}

TextBlock::TextBlock(wstring text,int color)
{
    textBlock = NULL;
    blockHeight = 1;
    blockWidth = text.length();

    textBlock = new CHAR_INFO[blockWidth*blockHeight];

    for(unsigned int index=0;index<text.length();index++)
    {
        textBlock[index].Attributes = color;
        textBlock[index].Char.UnicodeChar = text[index];
    }
}

TextBlock::~TextBlock(void)
{
    if(textBlock != NULL)
        delete [] textBlock;
}

CHAR_INFO *TextBlock::getTextBlock()
{
    return textBlock;
}