#include "AudioEngine.h"
#include <wchar.h>


AudioEngine::AudioEngine(void)
{
}


AudioEngine::~AudioEngine(void)
{
}

/*
SND_APPLICATION
SND_ALIAS	
SND_ALIAS_ID	
SND_ASYNC	
SND_LOOP	
SND_MEMORY	
SND_NOSTOP	
SND_PURGE	
SND_RESOURCE	
SND_SENTRY	
SND_SYNC	
SND_SYSTEM	
*/

void AudioEngine::jouerWaveChannel1(LPCWSTR fichier,int flags)
/*
    T�che: jouer un son Wave
    Param�tres: fichier a jouer
                flags de parametrage
*/
{
    PlaySound(fichier, NULL, SND_FILENAME | SND_NODEFAULT | flags);
}

void AudioEngine::arreterWaveChannel1()
/*
    T�che: arreter le son wave
*/
{
    PlaySound(NULL,0,0);
}

void AudioEngine::jouerMP3Channel2(LPCWSTR fichier,int startFrom)
/*
    T�che: jouer un mp3
    Param�tres: fichier a jouer
*/
{
    MCIERROR result;

    WCHAR buffer[100];

    swprintf_s(buffer,100,L"open %s type mpegvideo alias myMp3",fichier);

    //Liberer la resource de son
    mciSendString(L"close myMp3",NULL,0,NULL);

    //Ouvrir la resource de son
    result = mciSendString(buffer,NULL,0,NULL);

    swprintf_s(buffer,100,L"play myMp3 from %d",startFrom);

    //Jouer le son
    result = mciSendString(buffer,NULL,0,NULL);

    if(result != 0)
        return;
}

void AudioEngine::arreterMP3Channel2()
/*
    T�che: arreter le mp3
*/
{
    //Liberer la resource de son
    mciSendString(L"close myMp3",NULL,0,NULL);
}

int AudioEngine::getCurrentMP3Millisecond()
{
    WCHAR buffer[100];

    mciSendString(L"status myMp3 position",buffer,99,NULL);
    return _wtoi(buffer);    
}

void AudioEngine::jouerWaveChannel2(LPCWSTR fichier)
/*
    T�che: jouer un wave
    Param�tres: fichier a jouer
*/
{
    MCIERROR result;

    WCHAR buffer[100];

    //swprintf_s(buffer,100,L"open waveaudio!%s alias myWave",fichier);
    swprintf_s(buffer,100,L"open %s alias myWave",fichier);

    //Liberer la resource de son
    mciSendString(L"close myWave",NULL,0,NULL);

    //Ouvrir la resource de son
    result = mciSendString(buffer,NULL,0,NULL);

    //Jouer le son
    result = mciSendString(L"play myWave",NULL,0,NULL);

    //result = mciSendString(L"set myWave speed 2000",NULL,0,NULL);    
}

void AudioEngine::arreterWaveChannel2()
/*
    T�che: arreter le wave
*/
{
    //Liberer la resource de son
    mciSendString(L"close myWave",NULL,0,NULL);
}