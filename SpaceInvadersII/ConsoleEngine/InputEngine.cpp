#include "InputEngine.h"
#include "ConsoleEngine.h"
#include <sstream>
using namespace std;

InputEngine::InputEngine(void)
{
    hStdin = GetStdHandle(STD_INPUT_HANDLE); 
    GetConsoleMode(hStdin, &fdwSaveOldMode);
    fdwMode = ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT; 
    SetConsoleMode(hStdin, fdwMode);
    controleType = 0;
}

InputEngine::~InputEngine(void)
{
    SetConsoleMode(hStdin, fdwSaveOldMode);
}

void InputEngine::viderInputBuffer()
/*
    T�che: vider le buffer input de la console
*/
{
    HANDLE hstdin  = GetStdHandle( STD_INPUT_HANDLE  );
    FlushConsoleInputBuffer( hstdin );
}

void InputEngine::checkInput(int elapseTicks,ICallbackInput *screen)
{
    GetNumberOfConsoleInputEvents(hStdin,&cNumRead);

    if(cNumRead > 0)
    {
        ReadConsoleInput( 
                hStdin,      // input buffer handle 
                irInBuf,     // buffer to read into 
                128,         // size of read buffer 
                &cNumRead);  // number of records read       
    }
         

    // Dispatch the events to the appropriate handler. 
 
    for (i = 0; i < cNumRead; i++) 
    {
        switch(irInBuf[i].EventType) 
        { 
            case KEY_EVENT: // keyboard input 
                KeyEventProc(irInBuf[i].Event.KeyEvent,elapseTicks,screen); 
                break; 
 
            case MOUSE_EVENT: // mouse input 
                //MouseEventProc(irInBuf[i].Event.MouseEvent,elapseTicks,screen); 
                break; 
 
            case WINDOW_BUFFER_SIZE_EVENT: // scrn buf. resizing 
                //ResizeEventProc( irInBuf[i].Event.WindowBufferSizeEvent,elapseTicks,screen ); 
                break; 
 
            case FOCUS_EVENT:  // disregard focus events 
 
            case MENU_EVENT:   // disregard menu events 
                break; 
 
            default: 
                //ErrorExit("Unknown event type"); 
                break; 
        } 
    }
}

VOID InputEngine::KeyEventProc(KEY_EVENT_RECORD ker,int elapseTicks,ICallbackInput *screen)
{    
    if(ker.bKeyDown)
    {
		screen->key_pressed(elapseTicks,ker.wVirtualKeyCode);
    }
    else
	{
		screen->key_released(elapseTicks,ker.wVirtualKeyCode);
	}

/*    switch(ker.wVirtualKeyCode)
    {
        //switch keyboard from mouse
    case KEY_F12:
        {
            if(ker.bKeyDown)
            {
            }
            else
            {
                if(controleType == 0)
                {
                    controleType = 1;
                }
                else
                {
                    controleType = 0;
                }
            }
        }
        break;
    }*/
}

VOID InputEngine::MouseEventProc(MOUSE_EVENT_RECORD mer,int elapseTicks,ICallbackInput *screen)
{
#ifndef MOUSE_HWHEELED
#define MOUSE_HWHEELED 0x0008
#endif
    //OutputDebugString(L"Mouse event: ");
    
    switch(mer.dwEventFlags)
    {
        case 0:

            if(mer.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED)
            {
                OutputDebugString(L"left button press \n");
            }
            else if(mer.dwButtonState == RIGHTMOST_BUTTON_PRESSED)
            {
                OutputDebugString(L"right button press \n");
            }
            else
            {
                OutputDebugString(L"button press\n");
            }
            break;
        case DOUBLE_CLICK:
            OutputDebugString(L"double click\n");            
            break;
        case MOUSE_HWHEELED:
            OutputDebugString(L"horizontal mouse wheel\n");            
            break;
        case MOUSE_MOVED:
            dwMousePosition = mer.dwMousePosition;
            //OutputDebugString(L"mouse moved\n");            
            break;
        case MOUSE_WHEELED:
            OutputDebugString(L"vertical mouse wheel\n");            
            break;
        default:
            OutputDebugString(L"unknown\n");
            break;
    }
}

VOID InputEngine::ResizeEventProc(WINDOW_BUFFER_SIZE_RECORD wbsr,int elapseTicks,ICallbackInput *screen)
{
    OutputDebugString(L"Resize event\n");
    OutputDebugString(L"Console screen buffer is %d columns by %d rows.\n");
}

int InputEngine::key_pressed(int vk)
{
    return (GetAsyncKeyState(vk) != 0);
}

int InputEngine::getKeyboardInput()
{
    if(GetAsyncKeyState(VK_RIGHT))
    {
        return 0x4d;
    }

    if(GetAsyncKeyState(VK_LEFT))
    {
        return 0x4b;
    }

    return -1;
}

COORD InputEngine::getMousePosition()
{
    return dwMousePosition;
}