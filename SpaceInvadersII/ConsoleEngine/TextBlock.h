#pragma once
#include <Windows.h>
#include <string>
using namespace std;

class TextBlock
{
private:
    CHAR_INFO *textBlock;
    int blockWidth;
    int blockHeight;

public:
    TextBlock(wstring stringblock[],int blockLines,int color);
    TextBlock(wstring text,int color);
    ~TextBlock(void);

    const int getBlockWidth() { return blockWidth; }
    const int getBlockHeight() { return blockHeight; }
    CHAR_INFO *getTextBlock();
};

