#include "../resource.h"
#include "ConsoleEngine.h"

#include "DisplayEngine.h"
#include <sstream>
using namespace std;

#pragma comment( lib, "user32.lib" )

DisplayEngine::DisplayEngine(int width,int height)
{
    BOOL result;

    frameCount = 0;

    this->consoleWidth = width;
    this->consoleHeight = height;

    buffer = new CHAR_INFO[width*height];

    //Get a handle on the console window
    hOutput = (HANDLE)GetStdHandle( STD_OUTPUT_HANDLE ); 

    CONSOLE_FONT_INFOEX terminalFont;
    //adjust the font
	wcscpy_s(terminalFont.FaceName,sizeof(terminalFont.FaceName)/2,L"Terminal");
	terminalFont.cbSize = sizeof(CONSOLE_FONT_INFOEX);
	terminalFont.FontFamily = 48;
	terminalFont.FontWeight = 400;
	terminalFont.nFont = 2;
	terminalFont.dwFontSize.X = 8;
	terminalFont.dwFontSize.Y = 8;
	result = SetCurrentConsoleFontEx(hOutput,FALSE,&terminalFont);

    //adjust the display buffer size
    dwBufferSize.X = this->consoleWidth;
    dwBufferSize.Y = this->consoleHeight;
    dwBufferCoord.X = 0;
    dwBufferCoord.Y = 0;
    rcRegion.Left = 0;
    rcRegion.Top = 0;
    rcRegion.Right = this->consoleWidth-1;
    rcRegion.Bottom = this->consoleHeight-1;

    
    result = SetConsoleOutputCP( 850 );	
	HWND console = GetConsoleWindow();    

    MoveWindow(console, 0, 0, 20, 20, TRUE);

    //adjust the display buffer size
    result = SetConsoleScreenBufferSize(hOutput,dwBufferSize);
	//adjust the display buffer size
    result = SetConsoleWindowInfo(hOutput,true,&rcRegion);
    //initialise the back buffer
    result = ReadConsoleOutput( hOutput, (CHAR_INFO *)buffer, dwBufferSize, dwBufferCoord, &rcRegion ); 

    //hide the cursor
    curseurVisible(false);
    //set the console title
	SetConsoleTitle(L"Space Invader II");
    //set the console icon
	result = SetConsoleIcon(LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON)));

	canDraw = true;
}

DisplayEngine::~DisplayEngine(void)
{
    delete [] buffer;
}

/*
* Set the icon of the console window
*/
BOOL WINAPI DisplayEngine::SetConsoleIcon(HICON hIcon) 
{
	typedef BOOL (WINAPI *PSetConsoleIcon)(HICON);
	static PSetConsoleIcon pSetConsoleIcon = NULL;
	if(pSetConsoleIcon == NULL)
		pSetConsoleIcon = (PSetConsoleIcon)GetProcAddress(GetModuleHandle(L"kernel32"), "SetConsoleIcon");
	if(pSetConsoleIcon == NULL)
		return FALSE;
	return pSetConsoleIcon(hIcon);
}

/*
* Get the center of the console window
*/
Coord DisplayEngine::getCenter()
{
    Coord coord;

    coord.setPositionX(consoleWidth/2);
    coord.setPositionY(consoleHeight/2);

    return coord;
}

/*
BOOL WINAPI DisplayEngine::ConsoleHandlerRoutine(DWORD dwCtrlType)
{
    ConsoleEngine *engine = ConsoleEngine::getInstance();
	switch(dwCtrlType)
	{
	case CTRL_CLOSE_EVENT:
		{
			engine->Display->CanDraw(false);
			return TRUE;
		}
		break;
	case CTRL_C_EVENT:
		{
			engine->Display->CanDraw(false);
			return TRUE;
		}
		break;
	case CTRL_BREAK_EVENT:
		{
			engine->Display->CanDraw(false);
			return TRUE;
		}
		break;
	case CTRL_LOGOFF_EVENT:
		{
			engine->Display->CanDraw(false);
			return TRUE;
		}
		break;
	case CTRL_SHUTDOWN_EVENT:
		{
			engine->Display->CanDraw(false);
			return TRUE;
		}
		break;
	}

	return FALSE;
}
*/

/*
* Clear the console with the selected color
*/
void DisplayEngine::clearBuffer(int color)
{   
    int sizeBuffer = sizeof(CHAR_INFO)*consoleHeight*consoleWidth;

    //memcpy_s(buffer,sizeBuffer,bufferblank,sizeBuffer);

    for(int x=0;x<consoleWidth;x++)
    {
        for(int y=0;y<consoleHeight;y++)
        {
			buffer[y*consoleWidth+x].Char.UnicodeChar = ' '; 
            buffer[y*consoleWidth+x].Attributes = color; 
        }
    }

    //WriteConsoleOutput( hOutput, (CHAR_INFO *)buffer, dwBufferSize, dwBufferCoord, &rcRegion ); 
}

/*
* write a line in the back buffer
*/
void DisplayEngine::writeLine(int x1,int y1,int x2 ,int y2,int color)
{
    if(y1 == y2)
    {
        for(int x=x1;x<x2;x++)
        {
			buffer[y1*consoleWidth+x].Char.UnicodeChar = ' '; 
            buffer[y1*consoleWidth+x].Attributes = color; 
        }
    }
    if(x1 == x2)
    {
        for(int y=y1;y<y2;y++)
        {
			buffer[y*consoleWidth+x1].Char.UnicodeChar = ' '; 
            buffer[y*consoleWidth+x1].Attributes = color; 
        }
    }
}

/*
* Write text in the back buffer
*/
void DisplayEngine::writeText(int x,int y,int color, const wchar_t character)
{
	if(y < 0)
		return;

	if(y > consoleHeight)
		return;

    //Clipper le text a gauche
    if(x < 0)
    {
        return;
    }

    if(x > consoleWidth)
    {
        return;
    }

    buffer[y*consoleWidth+x].Char.UnicodeChar = character; 
    buffer[y*consoleWidth+x].Attributes = color; 
}

/*
* Write text in the back buffer
*/
void DisplayEngine::writeText(int x,int y,int color, const wstring &text)
{
    int stringlenght = text.length();
    int clippingRight = 0;
    int clippingLeft = 0;

	if(y < 0)
		return;

	if(y > consoleHeight)
		return;

    //Clipper le text a gauche
    if(x < 0)
    {
        if(x > stringlenght)
            clippingLeft = stringlenght;

        clippingLeft = abs(x);
    }

    //Clipper le text a droite
    if((consoleWidth - (stringlenght+x)) <0)
        clippingRight = (stringlenght+x) - consoleWidth;

    for(int i=clippingLeft;i < stringlenght-clippingRight;i++)
    {
        buffer[y*consoleWidth+x+i].Char.UnicodeChar = text[i]; 
        buffer[y*consoleWidth+x+i].Attributes = color; 
    }
}

/*
* write a block of text in the back buffer
*/
void DisplayEngine::displayTextBlock(int positionX,int positionY,TextBlock *textBlock)
{
    int clippingTop = 0;
    int clippingBottom = 0;
    int clippingRight = 0;
    int clippingLeft = 0;    

    //Clipper le haut du block 
    if(positionY < 0)
    {
        if(abs(positionY) > textBlock->getBlockHeight())
            clippingTop = textBlock->getBlockHeight();
        else
            clippingTop = abs(positionY);
    }

    //clipper le bas du block
    if(positionY+textBlock->getBlockHeight() > consoleHeight)
    {
        clippingBottom = consoleHeight - positionY + textBlock->getBlockHeight();
    }

    //Clipper a gauche du block
    if(positionX < 0)
    {
        if(abs(positionX) > textBlock->getBlockWidth())
            clippingLeft = textBlock->getBlockWidth();
        else
            clippingLeft = abs(positionX);
    }

    //Clipper a droite du block
    if((consoleWidth - (textBlock->getBlockWidth()+positionX)) < 0)
        clippingRight = consoleWidth - (textBlock->getBlockWidth()+positionX);

    for(int x = clippingLeft ; x < textBlock->getBlockWidth() + clippingRight ; x++)
    {
        for(int y = clippingTop ; y < textBlock->getBlockHeight() + clippingBottom ; y++)
        {
            buffer[(positionY+y)*consoleWidth + positionX+x].Attributes = textBlock->getTextBlock()[y*textBlock->getBlockHeight() + x].Attributes;
            buffer[(positionY+y)*consoleWidth + positionX+x].Char.UnicodeChar = textBlock->getTextBlock()[y*textBlock->getBlockWidth() + x].Char.UnicodeChar;
        }
    }
}

/*
* display a block in the back buffer
*/
void DisplayEngine::displayObject(int positionX,int positionY, const CHAR_INFO *objectBlock,int width,int height)
{
    int clippingTop = 0;
    int clippingBottom = 0;
    int clippingRight = 0;
    int clippingLeft = 0;

    //Clipper le haut du block 
    if(positionY < 0)
    {
        if(abs(positionY) > height)
            clippingTop = height;
        else
            clippingTop = abs(positionY);
    }

    //clipper le bas du block
    if(positionY+height > consoleHeight)
    {
        clippingBottom = consoleHeight - positionY + height;
    }

    //Clipper a gauche du block
    if(positionX < 0)
    {
        if(abs(positionX) > width)
            clippingLeft = width;
        else
            clippingLeft = abs(positionX);
    }

    //Clipper a droite du block
    if((consoleWidth - (width+positionX)) < 0)
        clippingRight = consoleWidth - (width+positionX);

    for(int x = clippingLeft ; x < width + clippingRight ; x++)
    {
        for(int y = clippingTop ; y < height + clippingBottom ; y++)
        {
            buffer[(positionY+y)*consoleWidth + positionX+x].Attributes = objectBlock[y*width + x].Attributes;
            buffer[(positionY+y)*consoleWidth + positionX+x].Char.UnicodeChar = objectBlock[y*width + x].Char.UnicodeChar;
        }
    }
}

/*
* blit the back buffer in the console buffer
*/
bool DisplayEngine::draw()
{
	BOOL result = false;
	
	if(canDraw)
	{
		result = WriteConsoleOutput( hOutput, (CHAR_INFO *)buffer, dwBufferSize, dwBufferCoord, &rcRegion ); 

        frameCount++;
	}
	else
	{
		int i=0;
	}
    //Convert BOOL to bool
	return result == 1;
}

/*
* set the cursor visibility
*/
void DisplayEngine::curseurVisible(bool visibilite)
/*
	T�che: Rend le curseur visible ou invisible
	Entr�e: visibilite = le curseur doit �tre invisible (VRAI ou FAUX)
*/
{
	CONSOLE_CURSOR_INFO curseur;
	GetConsoleCursorInfo (GetStdHandle(STD_OUTPUT_HANDLE), &curseur);
	curseur.bVisible = visibilite;
	SetConsoleCursorInfo (GetStdHandle(STD_OUTPUT_HANDLE), &curseur);
}