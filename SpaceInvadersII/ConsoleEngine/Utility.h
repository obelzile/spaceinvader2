#pragma once

#include <Windows.h>
#include <time.h>
#include <stdlib.h>
#include <string>
using namespace std;

struct RECTANGLE
{
	int top;
	int left;
	int width;
	int height;
	RECTANGLE(int left,int top,int width,int height) : top(top),left(left),width(width),height(height) {}

	bool isInside(int positionX,int positionY)
	{
		if(positionX >= left && positionX <= left + width
			&& positionY >= top && positionY <= top + height)
			return true;
		return false;
	}

};

enum Color
{
    Black       = 0,
    Grey        = FOREGROUND_INTENSITY,
    LightGrey   = FOREGROUND_RED   | FOREGROUND_GREEN | FOREGROUND_BLUE,
    White       = FOREGROUND_RED   | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
    Blue        = FOREGROUND_BLUE,
    Green       = FOREGROUND_GREEN,
    Cyan        = FOREGROUND_GREEN | FOREGROUND_BLUE,
    Red         = FOREGROUND_RED,
    Purple      = FOREGROUND_RED   | FOREGROUND_BLUE,
    LightBlue   = FOREGROUND_BLUE  | FOREGROUND_INTENSITY,
    LightGreen  = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
    LightCyan   = FOREGROUND_GREEN | FOREGROUND_BLUE  | FOREGROUND_INTENSITY,
    LightRed    = FOREGROUND_RED   | FOREGROUND_INTENSITY,
    LightPurple = FOREGROUND_RED   | FOREGROUND_BLUE  | FOREGROUND_INTENSITY,
    Orange      = FOREGROUND_RED   | FOREGROUND_GREEN,
    Yellow      = FOREGROUND_RED   | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
};

class Utility
{    
public:
    static bool initialiser;
    static int intervalAleatoire(int min,int max);
	static string charToOem(const wstring &texte);
    static int getCurrentTick();
    static int makeColor(Color fgColor,Color bgColor);
};