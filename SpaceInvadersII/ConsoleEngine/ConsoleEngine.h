#pragma once

#include "DisplayEngine.h"
#include "AudioEngine.h"
#include "InputEngine.h"
#include "Utility.h"

class AudioEngine;
class InputEngine;
class DisplayEngine;

class ConsoleEngine
{
private:
    ConsoleEngine();
    static ConsoleEngine* instance;

public:    
    AudioEngine *Audio;
    InputEngine *Input;
    DisplayEngine *Display;

    void initialise(int width,int height);
    ~ConsoleEngine(void);

    static ConsoleEngine* getInstance();
};
