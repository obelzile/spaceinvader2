#include "ConsoleEngine.h"


ConsoleEngine::ConsoleEngine()
{
}

ConsoleEngine::~ConsoleEngine(void)
{
    delete Input;
    delete Audio;
    delete Display;
}

ConsoleEngine* ConsoleEngine::getInstance()
{
    if(!instance)
        instance = new ConsoleEngine;

    return instance;
}

void ConsoleEngine::initialise(int width,int height)
{
    Display = new DisplayEngine(width,height);
    Audio = new AudioEngine();
    Input = new InputEngine();    
}