#pragma once
#include "consoleengine\textblock.h"
#include "ConsoleEngine\ConsoleEngine.h"

#include <string>
using namespace std;

class BlinkingText :
    public TextBlock
{
private:
    int colors[6];
    int colorChangeDelay;
    int currentTickCount;
    int currentColorIndex;
	bool blinkEnabled;

public:
    BlinkingText(wstring textblock[],int blockLines,int color,int colorChangeDelay);
    BlinkingText(wstring text,int color,int colorChangeDelay);
    ~BlinkingText(void);

	const bool enabled() const { return blinkEnabled; }
	void enabled(bool value) { blinkEnabled = value; }
    bool applyColor(int color);
    bool update(int elapsedTick,ConsoleEngine *engine);
    bool render(ConsoleEngine *engine);
};

