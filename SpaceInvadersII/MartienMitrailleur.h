#pragma once

#include "GameEngine.h"
#include "Martien.h"

class MartienMitrailleur : public Martien
{
private:
public:
    MartienMitrailleur(int positionX,int positionY,int fireDelay, int type,int valeur,GameEngine *game);
    ~MartienMitrailleur(void);
	bool canFire(int elapsedTick);
    bool fireLaser();
    bool collisionTest(int positionX,int positionY);
    bool update(int elapsedTicks, ConsoleEngine *engine);
	bool render(ConsoleEngine *engine);
};

