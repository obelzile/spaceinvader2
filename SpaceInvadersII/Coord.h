#pragma once

class Coord
{
    protected:
	    int posX;
	    int posY;
	
	public:			
	    Coord();
	    int getPositionX();
	    int getPositionY();
	    void setPositionX(int);
	    void setPositionY(int);
};