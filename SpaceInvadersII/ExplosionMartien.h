#pragma once
#include "Explosion.h"

class ExplosionMartien : public Explosion
{
public:
	ExplosionMartien(int positionX,int positionY,int timeToLive);
    ExplosionMartien(int positionX,int positionY,int timeToLive,wstring frames[]);
};

