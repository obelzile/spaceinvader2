#include "LaserMartien.h"
#include <string>
using namespace std;


LaserMartien::LaserMartien(int startX,int startY,wstring laserChar,int color,int speed)
{
	coord.setPositionX(startX);
	coord.setPositionY(startY);

	this->laserChar = laserChar;
	this->color = color;
	this->speed = speed;
	currentTickCount = 0;
}

LaserMartien::~LaserMartien(void)
{
}

bool LaserMartien::testCollision(GameEngine *game)
{
	//if(coord.getPositionX() )
	return true;
}
   
bool LaserMartien::update(int elapsedTicks,ConsoleEngine *engine)
{
	currentTickCount += elapsedTicks;
	if(currentTickCount >= speed)
	{
		currentTickCount = 0;
		if(coord.getPositionY() < engine->Display->ConsoleHeight()-1)
		{
			coord.setPositionY(coord.getPositionY()+1);
		}
		else
		{
			return false;
		}
	}

	//collision vaisseau

	return true;
}

bool LaserMartien::render(ConsoleEngine *engine)
{
	engine->Display->writeText(coord.getPositionX(),coord.getPositionY(),color,laserChar);

	return true;
}