#pragma once

#include "GameEngine.h"
#include "laser.h"
#include "GameEngine.h"

class GameEngine;

class LaserMartien :
	public Laser
{
private:
	wstring laserChar;
	int color;
	int speed;
	int currentTickCount;

public:
	LaserMartien(int startX,int startY,wstring laserChar,int color,int speed);
	~LaserMartien(void);
	
	bool testCollision(GameEngine *game);
    bool update(int elapsedTicks,ConsoleEngine *game);
	bool render(ConsoleEngine *game);
};

