﻿#include "Martien.h"
#include "ConsoleEngine/Utility.h"

Martien::Martien(int fireDelay,int type,int valeur,GameEngine *game)
	: GameObject()
{
	this->game = game;
	this->fireDelay = fireDelay;
	this->currentFireDelay = 0;
    this->scoreValue = valeur;
}

Martien::~Martien(void)
{
	delete frames;
}

RECTANGLE Martien::getRect()
{
	return RECTANGLE(coord.getPositionX(),coord.getPositionY(),frames->getWidth(),frames->getHeight());
}

bool Martien::moveXofOffset(int positionX)
{
    frames->nextFrame();
    coord.setPositionX(coord.getPositionX()+positionX);
    return true;
}

bool Martien::moveYofOffset(int positionY)
{
    frames->nextFrame();
    coord.setPositionY(coord.getPositionY()+positionY);
    return true;
}

bool Martien::collisionTest(int positionX,int positionY)
{
	return getRect().isInside(positionX,positionY);
}
