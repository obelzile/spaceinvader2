#pragma once

#include "AnimationBlock.h"
//#include "GameEngine.h"
#include "GameObject.h"

class GameEngine;

class Martien :
	public GameObject
{
protected:
	GameEngine *game;
	AnimationBlock *frames;
	int fireDelay;
	int currentFireDelay;
    int scoreValue;

public:
	Martien(int fireDelay, int type, int scoreValue, GameEngine *game);
	virtual ~Martien(void);

	RECTANGLE getRect();
    bool setSpeed(int speed) { frames->frameSpeed(speed); }
    bool moveXofOffset(int positionX);
    bool moveYofOffset(int positionY);
	bool collisionTest(int positionX,int positionY);
    int getScoreValue() { return scoreValue; }
    virtual bool fireLaser()=0;
	virtual bool canFire(int elapsedTick) =0;
	//virtual bool collisionTest(int positionX,int positionY) = 0;
};

