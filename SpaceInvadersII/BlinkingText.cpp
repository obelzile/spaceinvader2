#include "BlinkingText.h"

/*
* Block of Text that blink
*/

BlinkingText::BlinkingText(wstring textblock[],int color,int blockLines,int colorChangeDelay)
    : TextBlock(textblock,color,blockLines)
{
    colors[0] = 15;
    colors[1] = 7;
    colors[2] = 8;
    colors[3] = 0;
    colors[4] = 8;
    colors[5] = 7;    

    this->colorChangeDelay = colorChangeDelay;
    currentColorIndex = 0;
    currentTickCount = 0;
	blinkEnabled = true;
}

BlinkingText::BlinkingText(wstring text,int color,int colorChangeDelay)
    : TextBlock(text,color)
{
    colors[0] = 15;
    colors[1] = 7;
    colors[2] = 8;
    colors[3] = 0;
    colors[4] = 8;
    colors[5] = 7;    

    this->colorChangeDelay = colorChangeDelay;
    currentColorIndex = 0;
    currentTickCount = 0;
	blinkEnabled = true;
}


BlinkingText::~BlinkingText(void)
{

}

bool BlinkingText::applyColor(int color)
{
    for(int width=0;width<this->getBlockWidth();width++)
    {
        for(int height=0;height<this->getBlockHeight();height++)
        {
            this->getTextBlock()[height*this->getBlockWidth() + width].Attributes = color;
        }
    }
    return true;
}

bool BlinkingText::update(int elapsedTick,ConsoleEngine *engine)
{
	if(enabled())
	{
		currentTickCount += elapsedTick;
		if(currentTickCount >= colorChangeDelay)
		{
			currentTickCount = 0;
			currentColorIndex++;
			if(currentColorIndex > 5)
				currentColorIndex = 0;

			applyColor(colors[currentColorIndex]);
		}
	}
    return true;
}

bool BlinkingText::render(ConsoleEngine *engine)
{
    engine->Display->displayTextBlock(20,5,this);

    return true;
}