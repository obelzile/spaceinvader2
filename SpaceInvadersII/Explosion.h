#pragma once

#include "AnimationBlock.h"
#include "GameObject.h"

class Explosion : 
    GameObject
{
protected:
    int timeToLive;

    AnimationBlock *frames;
public:
	Explosion(int positionX,int positionY,int timeToLive);
    Explosion(int positionX,int positionY,int timeToLive,wstring frames[]);
	~Explosion(void);
    AnimationBlock *getExplosionFrames() { return frames; } 
	bool update(int elapsedTicks,ConsoleEngine *engine);
	bool render(ConsoleEngine *engine);
};