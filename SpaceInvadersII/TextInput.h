#pragma once
#include <string>
using namespace std;

#include "Coord.h"

class TextInput
{
private:
    wstring inputText;
    bool inputActif;
    int indexActif;
    wchar_t currentCharacter;
    int maxCharacter;
public:
    Coord coord;

    TextInput(int maxCharacter,int positionX,int positionY);
    ~TextInput(void);
    bool isActive() { return inputActif; }
    wstring getInput() { return inputText; }
    bool initialise();
    bool dispose();
    bool update(int elapsedTick);
    bool render();
	bool key_pressed(int elapsedTick,int virtualKey);
	bool key_released(int elapsedTick,int virtualKey);

};

