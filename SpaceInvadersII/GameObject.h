#pragma once

#include "Coord.h"
#include "ConsoleEngine\ConsoleEngine.h"

/*
* Object base class
*/
class GameObject
{
public:

    Coord coord;

    virtual bool update(int elapsedTicks, ConsoleEngine *engine) = 0;
	virtual bool render(ConsoleEngine *engine) = 0;
};

