#pragma once
#include <string>
using namespace std;
#include "laser.h"
#include "GameEngine.h"
#include "Martien.h"

class GameEngine;

class ShipLaser :
    public Laser
{
private:
    wstring laserChar;
    int color;
    int speed;
    int currentTickCount;

public:
    ShipLaser(int startX,int startY,wstring laserChar,int color,int speed);
    ~ShipLaser(void);
	bool testCollisions(GameEngine *game);

    bool update(int elapsedTick,ConsoleEngine *engine);
	bool render(ConsoleEngine *engine);
};

