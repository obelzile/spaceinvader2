#include "AnimationBlock.h"

/*
* Block of Text that is animated
*/

AnimationBlock::AnimationBlock(int positionX,int positionY,int frameChangeSpeed,bool loop)
{
	loopFrame = loop;
	currentTicksCount = 0;
	currentFrame = 0;
	this->frameChangeSpeed = frameChangeSpeed;
	blinkEnabled = true;
	this->positionX = positionX;
	this->positionY = positionY;
}

AnimationBlock::~AnimationBlock(void)
{
	while(blockFrames.size() !=0)
	{
		delete blockFrames[blockFrames.size()-1];
		blockFrames.pop_back();
	}
}

/*
* Add textblock frame to the animation
*/
void AnimationBlock::addFrame(TextBlock *frame)
{
	blockFrames.push_back(frame);
}

/*
* Add string table as a frame
*/
void AnimationBlock::addFrame(wstring frame[],int nbLines,int color)
{
    //Create a TextBlock with the string table
	blockFrames.push_back(new TextBlock(frame,nbLines,color));
}

/*
* Add string line as a frame
*/
void AnimationBlock::addFrame(wstring frame,int color)
{
    blockFrames.push_back(new TextBlock(frame,color));
}

/*
* update the animation 
*/
bool AnimationBlock::update(int elapsedTick,ConsoleEngine *engine)
{
	if(!enabled())
		return false;

	currentTicksCount += elapsedTick;

    //progress frame if the delay is passed
	if(currentTicksCount >= frameChangeSpeed)
	{
		currentTicksCount = 0;
		currentFrame++;

        //if at last frame 
		if(currentFrame > (int)blockFrames.size()-1)
            //loop to beginning
			if(loopFrame)
				currentFrame = 0;
			else // Stop animation
			{
				enabled(false);
				currentFrame = blockFrames.size()-1;
			}
	}

	return true;
}

/*
* set the animation to the next frame
*/
bool AnimationBlock::nextFrame()
{
	currentFrame++;
	if(currentFrame > (int)blockFrames.size()-1)
    {
		if(loopFrame)
			currentFrame = 0;
		else
		{
			enabled(false);
			currentFrame = blockFrames.size()-1;
		}
    }

    return true;
}

/*
* render the current frame of the animation
*/
bool AnimationBlock::render(ConsoleEngine *engine)
{
	engine->Display->displayTextBlock(positionX,positionY,blockFrames[currentFrame]);
	return true;
}
