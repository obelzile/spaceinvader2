#pragma once
#include "Explosion.h"

class ExplosionLaser : public Explosion
{
private:
public:
	ExplosionLaser(int positionX,int positionY,int timeToLive);
    ExplosionLaser(int positionX,int positionY,int timeToLive,wstring frames[]);
};

