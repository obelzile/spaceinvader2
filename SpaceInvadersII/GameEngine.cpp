﻿#pragma warning (disable : 4482)

#include "GameEngine.h"
#include "MartienMitrailleur.h"
#include "MartienBombardier.h"
#include "MartienArmy.h"

ConsoleEngine* ConsoleEngine::instance = NULL;
bool Utility::initialiser = false;

GameEngine::GameEngine(void)
{
	engine = ConsoleEngine::getInstance();
	engine->initialise(100,50);
    ship = NULL;
    martienArmy = NULL;
}

GameEngine::~GameEngine(void)
{
	dispose();
}

/*
* Initialise the game screens
*/
bool GameEngine::initialise()
{
	startScreen = new StartScreen(this);
	mainMenuScreen = new MainMenuScreen(this);
	highScoreScreen = new HighScoreScreen(this);
	gameplayScreen = new GamePlayScreen(this);
    pauseScreen = new PauseScreen(this);
	introScreen = new IntroScreen(this);

	setCurrentGameScreen(GameScreenEnum::Start);
    
    return true;
}

/*
* Dispose of the game resources
*/
bool GameEngine::dispose()
{
	while(explosions.size() !=0)
	{
		delete explosions[explosions.size()-1];
		explosions.pop_back();
	}

	while(laserMartiens.size() !=0)
	{
		delete laserMartiens[laserMartiens.size()-1];
		laserMartiens.pop_back();
	}

    if(ship != NULL)
	    delete ship;

    if(martienArmy != NULL)
        delete martienArmy;

	introScreen->dispose();
	delete introScreen;

	mainMenuScreen->dispose();
	delete mainMenuScreen;

	highScoreScreen->dispose();
	delete highScoreScreen;
	
	gameplayScreen->dispose();
	delete gameplayScreen;

    pauseScreen->dispose();
    delete pauseScreen;

    return true;
}

/*
* Set the current view
*/
bool GameEngine::setCurrentGameScreen(GameScreenEnum screen)
{
	switch(screen)
	{
	case GameScreenEnum::Quit:
		currentScreen = NULL;
		break;
	case GameScreenEnum::Start:
		currentScreen = startScreen;
		break;
	case GameScreenEnum::MainMenu:
		currentScreen = mainMenuScreen;
		break;
	case GameScreenEnum::Intro:
		currentScreen = introScreen;
		break;
	case GameScreenEnum::HighScore:
		currentScreen = highScoreScreen;        
		break;
	case GameScreenEnum::GamePlay:
		currentScreen = gameplayScreen;
		break;
    case GameScreenEnum::Pause:
        currentScreen = pauseScreen;
        break;
	}
	
	if(currentScreen != NULL && !currentScreen->isInitialised())
		currentScreen->initialise();

	return true;
}

/*
* Game loop
*/
bool GameEngine::run()
{
	int elapseTicks = 0;

	while(1)
	{
		currentTicks = Utility::getCurrentTick();
		elapseTicks = currentTicks - previousTicks;
		previousTicks = currentTicks;

		engine->Display->clearBuffer();       

		engine->Input->checkInput(elapseTicks,currentScreen);
		
		//Menu quit : quitter le jeu
		if(currentScreen == NULL)
			break;

		//update the screen objects
		currentScreen->update(elapseTicks);

		//render the screen to the buffer
		currentScreen->render();

		//Copy buffer to screen
		engine->Display->draw();

		Sleep(10);
	}

	return true;
}