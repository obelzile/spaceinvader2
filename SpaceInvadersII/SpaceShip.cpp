﻿#include "SpaceShip.h"
#include <sstream>
#include <string>
using namespace std;

SpaceShip::SpaceShip(GameEngine *game)
{
	this->game = game;


    wstring spaceship[] = { L"   |   ",                            
                            L"  /▓\\  ",
                            L"^▀↕▀↕▀^"};

    this->spaceship = new TextBlock(spaceship,3,Utility::makeColor(White,Black));

    shields = 1;
    maxShields = 5;
    maxLaser = 1;
    moveDelay = 15;
    fireDelay = 150;
    moveTickCount = 0;
    fireTickCount = 0;
    shipWidth = 7;
    shipHeight = 3;
    shipDead = false;

    this->coord.setPositionX(30);
    this->coord.setPositionY(game->engine->Display->ConsoleHeight()-shipHeight);   
}

SpaceShip::~SpaceShip(void)
{
    vector<ShipLaser*>::iterator it = activeLasers.begin();
    while (it != activeLasers.end())
    {
        it = activeLasers.erase(it);
    }

	delete spaceship;
}

/*
* get the bounding box of the ship
*/
RECTANGLE SpaceShip::getRect()
{
	return RECTANGLE(coord.getPositionX(),coord.getPositionY(),spaceship->getBlockWidth(),spaceship->getBlockHeight());
}

/*
* test collision of the ship against the martien lasers
*/
bool SpaceShip::testCollision()
{
	bool collision = false;

	//shiplaser collision
	vector<ShipLaser*>::iterator iterLaser = activeLasers.begin();
	while (iterLaser != activeLasers.end())
	{
		if((*iterLaser)->testCollisions(game))
		{
            //collision was found remove the laser
			delete (*iterLaser);
			iterLaser = activeLasers.erase(iterLaser);
			collision = true;
		}
		else
		{
			iterLaser++;
		}
	}

	return collision;
}

/*
* update ship  
*/
bool SpaceShip::update(int elapsedTicks,ConsoleEngine *engine)
{   	
	RECTANGLE shipRect = getRect();

    //powerup collision  -- should be moved to collision check
	vector<PowerUp*>::iterator iterPowerUp = game->powerUps.begin();
	while (iterPowerUp != game->powerUps.end())
	{
		if(shipRect.isInside((*iterPowerUp)->coord.getPositionX(),(*iterPowerUp)->coord.getPositionY()))
		{   
			game->score += 20;
            switch((*iterPowerUp)->getType())
            {
            case Shield:
                game->ship->setShields(game->ship->getShields()+1);
                break;
            case Laser:
                game->ship->setMaxLaser(game->ship->getMaxLaser()+1);
                break;
            }

			delete (*iterPowerUp);
			iterPowerUp = game->powerUps.erase(iterPowerUp);		
		}
		else
		{
			iterPowerUp++;
		}
	}

    // test collision with martien laser
	vector<LaserMartien*>::iterator iterAlienLaser = game->laserMartiens.begin();
	while (iterAlienLaser != game->laserMartiens.end())
	{
		if(shipRect.isInside((*iterAlienLaser)->coord.getPositionX(),(*iterAlienLaser)->coord.getPositionY()))
		{            
			delete (*iterAlienLaser);
			iterAlienLaser = game->laserMartiens.erase(iterAlienLaser);
            // No more shield
            if(game->ship->getShields() == 0)
            {
                shipDead = true;
                vector<ShipLaser*>::iterator it = activeLasers.begin();
                while (it != activeLasers.end())
                {
                    it = activeLasers.erase(it);
                }
			    return false;
            }
            else
                game->ship->setShields(game->ship->getShields()-1); //Loose 1 shield
		}
		else
		{
			iterAlienLaser++;
		}
	}

    //update les laser tirés par le vaisseau
    vector<ShipLaser*>::iterator it = activeLasers.begin();
    while (it != activeLasers.end())
    {
        if(!(*it)->update(elapsedTicks,game->engine))
        {            
            it = activeLasers.erase(it);
        }
        else
        {
            it++;
        }
    }
	
	moveTickCount += elapsedTicks;
	fireTickCount += elapsedTicks;

	//update le statue du vaisseau selon les input
	if(moveTickCount>=moveDelay)
	{
		moveTickCount = 0;
		ConsoleEngine* engine = ConsoleEngine::getInstance();

		if(game->engine->Input->getControlType()==0)
		{
			if(game->engine->Input->key_pressed(KEY_RIGHT_KEY))
			{
                //Move right
				deplacerDroite();
			}

			if(engine->Input->key_pressed(KEY_LEFT_KEY))
			{
                //Move left
				deplacerGauche();            
			}
		}
		else
		{        
            //move with mouse
			this->coord.setPositionX(game->engine->Input->getMousePosition().X);
		}
	}

    //Fire a laser if the delay is passed between fire a previous one
	if(fireTickCount >= fireDelay)
	{    
		fireTickCount = 0;

		if(game->engine->Input->key_pressed(KEY_SPACE_KEY))
		{
			fireLaser();          
		}
	}
	return true;
}


void SpaceShip::setMaxLaser(int value)
{
    maxLaser =value;
    if(maxLaser > 5)
        maxLaser = 5;
}

void SpaceShip::setShields(int value) 
{
    shields = value;
    if(shields > maxShields)
        shields = maxShields;
}

bool SpaceShip::render(ConsoleEngine *engine)
{   
    bool result = false;

    //render the ship laser
    for(vector<ShipLaser*>::iterator it = activeLasers.begin();it!= activeLasers.end();++it)
    {
        result = (*it)->render(game->engine);
    }
    //render the ship if not dead
    if(!shipDead)
        game->engine->Display->displayTextBlock(this->coord.getPositionX(),this->coord.getPositionY(),spaceship);    
    
    return result;
}

/*
* Move ship to the left
*/
void SpaceShip::deplacerGauche()
{
    int positionX = this->coord.getPositionX();

    if(positionX >= 1)
        this->coord.setPositionX(this->coord.getPositionX()-1);
}

/*
* Move ship to the left
*/
void SpaceShip::deplacerDroite()
{
    int positionX = this->coord.getPositionX();

    if(positionX <= game->engine->Display->ConsoleWidth()-1-this->shipWidth)
        this->coord.setPositionX(this->coord.getPositionX()+1);
}

/*
* Fire laser 
*/
void SpaceShip::fireLaser()
{
    //Check if you can fire a laser
    if((int)activeLasers.size() < maxLaser)
    {
        ShipLaser* laser = new ShipLaser(coord.getPositionX()+shipWidth/2,coord.getPositionY()+1,L"↑",Utility::makeColor(Yellow,Black),25);
       activeLasers.push_back(laser);
       game->engine->Audio->jouerWaveChannel2(L".\\Sounds\\shoot.wav");
    }
}

/*
* check if there is a collision with the ship
*/
bool SpaceShip::collisionTest(int positionX,int positionY)
{
    if(positionX >= coord.getPositionX() && positionX <= coord.getPositionX() + shipWidth
        && positionY >= coord.getPositionY() && positionY >= coord.getPositionY() + shipHeight)
        return true;
    
    return false;
}