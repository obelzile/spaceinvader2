﻿#include "ExplosionLaser.h"


ExplosionLaser::ExplosionLaser(int positionX,int positionY,int timeToLive)
    : Explosion(positionX,positionY,timeToLive)
{
    wstring frame1[] = { L"<▒█▒>"};
	wstring frame2[] = { L"▒▓+▓▒"};
	wstring frame3[] = { L"▓ • ▓"};
	wstring frame4[] = { L"     "};
	
	frames = new AnimationBlock(positionX,positionY,80,false);

	frames->addFrame(frame1,1,Utility::makeColor(Grey,Black));
	frames->addFrame(frame2,1,Utility::makeColor(Grey,Black));
	frames->addFrame(frame3,1,Utility::makeColor(Grey,Black));
	frames->addFrame(frame4,1,Utility::makeColor(Grey,Black));
	frames->enabled(true);
}