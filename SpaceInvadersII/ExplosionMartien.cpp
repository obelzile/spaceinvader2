﻿#include "ExplosionMartien.h"

ExplosionMartien::ExplosionMartien(int positionX,int positionY,int timeToLive)
    : Explosion(positionX,positionY,timeToLive)
{
	wstring frame[] = { L"\\ /",
						L"/ \\"};
	
	frames = new AnimationBlock(positionX,positionY,80,false);

	frames->addFrame(frame,2,Utility::makeColor(Yellow,Black));
	frames->enabled(true);
}