﻿
#include <sstream>
using namespace std;
#include "PowerUp.h"
#include "ConsoleEngine\ConsoleEngine.h"
#include "GameEngine.h"

PowerUp::PowerUp(int positionX,int positionY,PowerUpType type, int fallSpeed) :
        type(type),fallSpeed(fallSpeed)
{
    currentTicksCount = 0;
    coord.setPositionX(positionX);
    coord.setPositionY(positionY);
    
    wchar_t typeChar;
    int color;
    switch(type)
    {
    case Shield:
        color = Utility::makeColor(LightBlue,Black);
        typeChar = 'S';
        break;
    case Laser:
        color = Utility::makeColor(Yellow,Black);
        typeChar = 'L';
        break;
    }

    wstringstream frame1; 
    frame1 << L"▀" << typeChar << L"▀";

    wstringstream frame2; 
    frame2 << L"▄" << typeChar << L"▄";

    frames = new AnimationBlock(positionX,positionY,fallSpeed,true);
    frames->addFrame(frame1.str(),color);
    frames->addFrame(frame2.str(),color);
}


PowerUp::~PowerUp(void)
{
}


bool PowerUp::initialise()
{

    return true;
}

/*
* dispose of the frames
*/
bool PowerUp::dispose()
{
    if(frames != NULL)
        delete frames;

    return true;
}

/*
* Update the position and animation fo the powerup
*/
bool PowerUp::update(int elapsedTick,ConsoleEngine *engine)
{
    currentTicksCount +=elapsedTick;
    if(currentTicksCount >= fallSpeed)
    {
        //delay of speed is up, update the position
        currentTicksCount = 0;
		if(coord.getPositionY()<engine->Display->ConsoleHeight()-1)
		{
            //update position
			coord.setPositionY(coord.getPositionY()+1);
            frames->setPositionY(coord.getPositionY());
		}
		else
		{
            //Power outside the display zone , false will dispose of it
			return false;
		}
    }
    //Update animation frame
    frames->update(elapsedTick,engine);
    return true;
}

/*
* render the current powerup frame
*/
bool PowerUp::render(ConsoleEngine *engine)
{
    frames->render(engine);
    return true;
}