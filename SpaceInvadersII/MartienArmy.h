#pragma once

#include <vector>
using namespace std;

#include "Martien.h"
#include "GameEngine.h"

class MartienArmy
{
private:
    GameEngine *game;
    int speed;
    int currentTicksCount;
    bool direction;

    //sound timing
    int currentSoundTicks;
    int lastSoundTicks;
    int soundStep;

public:
    MartienArmy(GameEngine * game);
    ~MartienArmy(void);

    vector<Martien *> martiens;

    bool initialise();
    bool dispose();
    void updateSpeed();
    bool moveArmyRight();
    bool moveArmyLeft();
    bool moveArmyDown();
    bool moveSound(int elapsedTick);
    bool update(int elapsedTick);
	bool render(ConsoleEngine *engine);
};

