#include "Explosion.h"

Explosion::Explosion(int positionX,int positionY,int timeToLive)
{
    this->coord.setPositionX(positionX);
    this->coord.setPositionY(positionY);

	this->timeToLive = timeToLive;
}

Explosion::~Explosion(void)
{
	delete frames;
}

bool Explosion::update(int elapsedTick,ConsoleEngine *engine)
{
	frames->update(elapsedTick,engine);

	timeToLive -= elapsedTick;
	if(timeToLive < 0)
		return false;

	return true;
}

bool Explosion::render(ConsoleEngine *engine)
{
	return frames->render(engine);
}