#include "ShipLaser.h"
#include "ConsoleEngine/Utility.h"
#include "ExplosionMartien.h"
#include "ExplosionLaser.h"

ShipLaser::ShipLaser(int startX,int startY,wstring laserChar,int color,int speed)
{
    coord.setPositionX(startX);
    coord.setPositionY(startY);

    this->laserChar = laserChar;
    this->color = color;
    this->speed = speed;
    currentTickCount = 0;
}

ShipLaser::~ShipLaser(void)
{
}

/*
* Test shiplaser collision with different game object
*/
bool ShipLaser::testCollisions(GameEngine *game)
{
	if(coord.getPositionY()-1 == 0)
		return true;

    //test martien collision
	vector<Martien*>::iterator iterAlien = game->martienArmy->martiens.begin();
	while (iterAlien != game->martienArmy->martiens.end())
	{           
		RECTANGLE rect = (*iterAlien)->getRect();
		if(rect.isInside(coord.getPositionX(),coord.getPositionY()))
		{
            RECTANGLE rect = (*iterAlien)->getRect();
            
            if(Utility::intervalAleatoire(0,100) <= 10)
            {
                int type = Utility::intervalAleatoire(0,1);
                game->powerUps.push_back(new PowerUp(rect.left+rect.width/2,rect.top+rect.height,(PowerUpType)type,50));
            }
			game->explosions.push_back(new ExplosionMartien((*iterAlien)->coord.getPositionX()+2,(*iterAlien)->coord.getPositionY(),500));

            game->ajouterScore((*iterAlien)->getScoreValue());
			
            delete (*iterAlien);
			iterAlien = game->martienArmy->martiens.erase(iterAlien);
            game->martienArmy->updateSpeed();
            game->engine->Audio->jouerWaveChannel2(L".\\Sounds\\invaderkilled.wav");

			return true;
		}
		else
		{
			iterAlien++;
		}
	}

	//test collision with martien laser
	vector<LaserMartien*>::iterator iterLaser = game->laserMartiens.begin();
	while(iterLaser != game->laserMartiens.end())
	{
		if((*iterLaser)->coord.getPositionX() == this->coord.getPositionX() && ((*iterLaser)->coord.getPositionY() == this->coord.getPositionY() || (*iterLaser)->coord.getPositionY() == this->coord.getPositionY()+1))
		{
			game->explosions.push_back(new ExplosionLaser((*iterLaser)->coord.getPositionX()-2,(*iterLaser)->coord.getPositionY(),500));
			delete (*iterLaser);
			iterLaser = game->laserMartiens.erase(iterLaser);
			return true;
		}
		else
		{
			iterLaser++;
		}
	}	

	return false;
}
    
/*
* Update position of the laser 
*/
bool ShipLaser::update(int elapsedTicks,ConsoleEngine *engine)
{
    currentTickCount += elapsedTicks;
    if(currentTickCount >= speed)
    {
        currentTickCount = 0;
        if(coord.getPositionY()>0)
        {
            coord.setPositionY(coord.getPositionY()-1);
        }
        else
        {
            //outside display zone , dispose of it
            return false;
        }
    }

    return true;
}

/*
* Render the laser
*/
bool ShipLaser::render(ConsoleEngine *engine)
{
    engine->Display->writeText(coord.getPositionX(),coord.getPositionY(),color,laserChar);

    return true;
}