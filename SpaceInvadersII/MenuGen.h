#pragma once
#include <vector>
#include <string>
using namespace std;

#include "ConsoleEngine/ConsoleEngine.h"
#include "Coord.h"

class MenuGen
{
private:
	int currentSelection;    
	vector <wstring> options;
	int delay;
	int currentTicksCount;
    Coord coord;
public:

	MenuGen(int positionX,int positionY);
	~MenuGen(void);

	int getPositionX() { return coord.getPositionX(); }
	int getPositionY() { return coord.getPositionY(); }
    void setPositionX(int value) { coord.setPositionX(value); }
    void setPositionY(int value) { coord.setPositionY(value); }

	const wstring getOption(int index) const;

	int GetCurrentSelection(){return currentSelection;}
	bool initialise();
    int getWidth();
	bool addOption(wstring option);
	bool key_pressed(int elaspeTicks, int virtualKey);
	bool key_released(int elaspeTicks, int virtualKey);
	bool update(int elaspeTicks);
	bool render(ConsoleEngine *engine);
};

