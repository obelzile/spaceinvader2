#pragma once
#include "GameEngine.h"
#include "martien.h"

class MartienOfficier :
    public Martien
{
public:
    MartienOfficier(int positionX,int positionY,int fireDelay, int type,int valeur,GameEngine *game);
    ~MartienOfficier(void);

	bool canFire(int elapsedTick);
    bool fireLaser();
    bool collisionTest(int positionX,int positionY);
    bool update(int elapsedTicks, ConsoleEngine *engine);
	bool render(ConsoleEngine *engine);
};

