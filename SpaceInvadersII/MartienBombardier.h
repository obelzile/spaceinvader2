#pragma once

#include "GameEngine.h"
#include "Martien.h"

class MartienBombardier :
	public Martien
{
private:
public:
	MartienBombardier(int positionX,int positionY,int fireDelay, int type,int valeur,GameEngine *game);
	~MartienBombardier(void);
	bool canFire(int elapsedTick);
    bool fireLaser();
    bool collisionTest(int positionX,int positionY);
    bool update(int elapsedTicks, ConsoleEngine *engine);
	bool render(ConsoleEngine *engine);
};