#include "Coord.h"
#include <windows.h>
#include <iostream>
using namespace std;

Coord::Coord()
{
	posX=0;
	posY=0;
}

int Coord::getPositionX()
{
	return posX;
}

int Coord::getPositionY()
{
	return posY;
}
void Coord::setPositionX(int x)
{
	posX=x;
}
void Coord::setPositionY(int y)
{
	posY=y;
}
