﻿#include "TextInput.h"
#include "ConsoleEngine\ConsoleEngine.h"

/*
* highscore Arcade style input 
*/
TextInput::TextInput(int maxCharacter,int positionX,int positionY)
    : maxCharacter(maxCharacter)
{    
    coord.setPositionX(positionX);
    coord.setPositionY(positionY);

    inputActif = true;
    indexActif = 0;
    currentCharacter = 'A';
}

TextInput::~TextInput(void)
{
}

bool TextInput::initialise()
{
    return true;
}

bool TextInput::dispose()
{
    return true;
}

bool TextInput::update(int elapsedTick)
{
    return true;
}

bool TextInput::render()
{
    ConsoleEngine * engine = ConsoleEngine::getInstance();
    if(inputActif)
    {
        //display the character selection
        engine->Display->writeText(coord.getPositionX()+indexActif,coord.getPositionY(),Utility::makeColor(White,Black),L"▲");
        engine->Display->writeText(coord.getPositionX()+indexActif,coord.getPositionY()+2,Utility::makeColor(White,Black),L"▼");
        engine->Display->writeText(coord.getPositionX()+indexActif,coord.getPositionY()+1,Utility::makeColor(White,Black),currentCharacter);
    }
    //display the character without selection
    engine->Display->writeText(coord.getPositionX(),coord.getPositionY()+1,Utility::makeColor(White,Black),inputText);
    
    
    return true;
}

bool TextInput::key_pressed(int elapsedTick,int virtualKey)
{
    return true;
}

bool TextInput::key_released(int elapsedTick,int virtualKey)
{
    switch(virtualKey)
    {
    case KEY_UP:
        currentCharacter++;
        if(currentCharacter > L'Z')
            currentCharacter = L'A';
        break;
    case KEY_DOWN:
        currentCharacter--;
        if(currentCharacter < L'A')
            currentCharacter = L'Z';
        break;
    case KEY_RETURN_KEY:
        inputText += currentCharacter;
        indexActif++;
        if(indexActif == maxCharacter)
            inputActif = false;
        break;
    }
    return true;
}